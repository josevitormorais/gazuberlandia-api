package main

// import (
// 	"fmt"

// 	"github.com/Rhymond/go-money"
// 	"github.com/shopspring/decimal"
// )

// func main() {

// 	pp2 := decimal.NewFromInt(10000)
// 	pp3 := decimal.NewFromInt(pp2.Abs().CoefficientInt64())
// 	pp4 := money.New(pp2.Abs().CoefficientInt64(), money.BRL)
// 	fmt.Println(pp2.Abs())
// 	fmt.Println(pp2.Abs())
// 	fmt.Println(pp3)
// 	fmt.Println(pp3.Abs())
// 	fmt.Println(pp4.Amount())
// 	fmt.Println(pp4.Absolute())
// 	fmt.Println(pp4.Currency().Formatter())
// 	fmt.Println(pp4.Currency())
// 	fmt.Println(pp4.Display())

// }

// import (
// 	"fmt"
// 	"math/rand"
// 	"time"
// )

// func main() {

// 	c := fanIn(Jose("Oi andre"), Andre("Oi jose"))

// 	for {
// 		select {
// 		case s := <-c:
// 			fmt.Printf("%s \n", s)
// 		case <-time.After(time.Millisecond):
// 			fmt.Println("bad")
// 			return
// 		}
// 	}

// 	fmt.Println("Finnaly!!")

// }

// func fanIn(fisrstMsg, secondMsg <-chan string) <-chan string {
// 	c := make(chan string)

// 	go func() {
// 		for {
// 			select {
// 			case s := <-fisrstMsg:
// 				c <- s
// 			case s := <-secondMsg:
// 				c <- s
// 			}
// 		}
// 	}()

// 	return c
// }

// func Andre(msg string) <-chan string {
// 	c := make(chan string)

// 	count := 0

// 	go func() {
// 		for {
// 			if count >= 10 {
// 				break
// 			}
// 			count++
// 			c <- fmt.Sprintf("%s : %d", msg, count)
// 			time.Sleep(time.Duration(rand.Intn(1e3)) * time.Millisecond)
// 		}
// 	}()

// 	return c

// }

// func Jose(msg string) <-chan string {
// 	c := make(chan string)

// 	count := 0

// 	go func() {
// 		for {
// 			if count >= 10 {
// 				break
// 			}
// 			count++
// 			c <- fmt.Sprintf("%s : %d", msg, count)
// 			time.Sleep(time.Duration(rand.Intn(1e3)) * time.Millisecond)
// 		}
// 	}()

// 	return c
// }
