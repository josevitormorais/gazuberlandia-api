package gazuberlandia

import (
	"context"
	"errors"
	"fmt"
)

const (
	INTERNAL            string = "internal"
	BAD_REQUEST         string = "invalid"
	NOT_FOUND           string = "not_found"
	NOT_IMPLEMENTED     string = "not_implemented"
	UNAUTHORIZED        string = "unauthorized"
	UNPROCESSABLEENTITY string = "unprocessableentity"
	CONFLICT            string = "conflict"
	InternalErrMsg      string = "Internal server error."
)

type AppError struct {
	Code    string
	Message string
	Err     error
}

func (e *AppError) Error() string {
	return fmt.Sprintf("Error: code=%s message=%s err=%v", e.Code, e.Message, e.Err)
}

func ErrorCode(err error) string {
	var e *AppError

	if err == nil {
		return ""
	}

	if errors.As(err, &e) {
		return e.Code
	}

	return INTERNAL
}

func ErrorMessage(err error) string {
	var e *AppError

	if err == nil {
		return ""
	}

	if errors.As(err, &e) {
		return e.Message
	}

	if errors.Is(err, context.DeadlineExceeded) {
		return "Timeout Exceeded"
	}

	return InternalErrMsg
}

func ErrorF(code, message string, err error) *AppError {
	return &AppError{
		Code:    code,
		Err:     err,
		Message: message,
	}
}
