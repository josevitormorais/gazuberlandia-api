package config

import "fmt"

type Config struct {
	PGdb        string   `env:"POSTGRES_DB"`
	PGuser      string   `env:"POSTGRES_USER"`
	PGpass      string   `env:"POSTGRES_PASSWORD"`
	PGhost      string   `env:"POSTGRES_HOST"`
	PGport      int      `env:"POSTGRES_PORT"`
	PGssl       string   `env:"POSTGRES_SSLMODE"`
	ENVIRONMENT string   `env:"ENVIRONMENT"`
	Cors        []string `env:"CORS"`
	Timeout     int64    `env:"TIMEOUT"`
	JwtSecret   string   `env:"JWT_SCRET"`
}

func (c *Config) PostgresUrl() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=%s", c.PGuser, c.PGpass, c.PGhost, c.PGport, c.PGdb, c.PGssl)
}

func (c *Config) PostgresDsn() string {
	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=%s TimeZone=America/Sao_Paulo", c.PGhost, c.PGuser, c.PGpass, c.PGdb, c.PGport, c.PGssl)
}
