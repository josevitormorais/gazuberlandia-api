package domain

import (
	"context"

	"github.com/rs/zerolog"
)

func Logger(logger *zerolog.Logger, ctx context.Context) *zerolog.Logger {
	log := logger.With().Str("requestID", ctx.Value("requestID").(string)).Logger()
	return &log
}
