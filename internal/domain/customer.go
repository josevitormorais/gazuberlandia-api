package domain

import (
	"context"
	"gazuberlandia/internal/datastore/postgres/models"
	"reflect"
	"time"
)

type CustomerService interface {
	CreateCustomer(ctx context.Context, customer *CreateCustomerRequest) error
	UpdateCustomer(ctx context.Context, customer *Customer, companyID, id uint) error
	DeleteCustomer(ctx context.Context, id uint) error
	DeleteCustomerAddress(ctx context.Context, id uint) error
	FindCustomerById(ctx context.Context, id uint) (*Customer, error)
	GetAllCustomer(ctx context.Context, companyID uint) (*[]Customer, error)
	SearchCustomer(ctx context.Context, query CustomerQueryParams, page, pageSize, companyID uint) (*[]Customer, error)
	SearchCustomerPaginated(ctx context.Context, page, pageSize, companyID uint, withCalc bool) (*[]Customer, error)
}

type CustomerQueryParams struct {
	Calc  bool
	Query string
}

type CreateCustomerRequest struct {
	Name       string `json:"name"`
	Email      string `json:"email"`
	Active     bool   `json:"active"`
	Phone      string `json:"phone"`
	Cpf        string `json:"cpf"`
	CompanyID  uint   `json:"company_id"`
	IsWhatsapp bool   `json:"is_whatsapp"`
	Address    struct {
		Street       string `json:"street"`
		Neighborhood string `json:"neighborhood"`
		City         string `json:"city"`
		State        string `json:"state"`
		Complement   string `json:"complement"`
		Number       uint   `json:"number"`
	}
}

type Customer struct {
	ID              uint                     `json:"id"`
	Name            string                   `json:"name"`
	Email           string                   `json:"email"`
	Active          bool                     `json:"active"`
	Phone           string                   `json:"phone"`
	Cpf             string                   `json:"cpf"`
	CompanyID       uint                     `json:"company_id"`
	IsWhatsapp      bool                     `json:"is_whatsapp"`
	AmountPurchased int                      `json:"amount_purchased"`
	PurchasedQty    int                      `json:"purchased_qty"`
	Address         *CustomerAddressResponse `json:"address"`
	CreatedAt       time.Time                `json:"created_at"`
	UpdatedAt       time.Time                `json:"updated_at"`
}

type CustomerAddressResponse struct {
	ID           uint      `json:"id"`
	Street       string    `json:"street"`
	Neighborhood string    `json:"neighborhood"`
	City         string    `json:"city"`
	State        string    `json:"state"`
	Complement   string    `json:"complement"`
	Number       uint      `json:"number"`
	CustomerID   uint      `json:"customer_id"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}

func CustomerResponseFromDataStore(c models.Customer) *Customer {
	cr := &Customer{
		ID:              c.ID,
		Active:          c.Active,
		Name:            c.Name,
		Email:           c.Email,
		Phone:           c.Phone,
		Cpf:             c.Cpf,
		CompanyID:       c.CompanyID,
		IsWhatsapp:      c.IsWhatsapp,
		AmountPurchased: c.AmountPurchased,
		PurchasedQty:    c.PurchasedQty,
		CreatedAt:       c.CreatedAt,
		UpdatedAt:       c.UpdatedAt,
	}

	if reflect.ValueOf(c.Address).IsNil() {
		return cr
	}

	cr.Address = &CustomerAddressResponse{
		ID:           c.Address.ID,
		Street:       c.Address.Street,
		Neighborhood: c.Address.Neighborhood,
		City:         c.Address.City,
		State:        c.Address.State,
		Complement:   c.Address.Complement,
		Number:       c.Address.Number,
		CustomerID:   c.Address.CustomerID,
		CreatedAt:    c.Address.CreatedAt,
		UpdatedAt:    c.Address.UpdatedAt,
	}

	return cr
}

func AllCustomerFromStore(c []models.Customer) *[]Customer {
	var cts []Customer

	for _, cs := range c {
		cts = append(cts, *CustomerResponseFromDataStore(cs))
	}

	return &cts
}
