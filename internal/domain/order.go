package domain

import (
	"context"
	"gazuberlandia/internal/datastore/postgres/models"
	"time"

	"github.com/gofrs/uuid"
)

type OrderService interface {
	CreateOrder(ctx context.Context, order *Order, companyID uint) error
	UpdateOrder(ctx context.Context, order *OrderUpdated, orderID uuid.UUID, userID, companyID uint) error
	FindOrderByID(ctx context.Context, orderID uuid.UUID, userID, companyID uint) (*OrderResponse, error)
	GetAllOrder(ctx context.Context, userID, companyID uint) ([]OrderResponse, error)
	SearchOrderPaginated(ctx context.Context, page, pageSize, userID, companyID uint) ([]OrderResponse, error)
	SearchOrder(ctx context.Context, q *OrderQueryParams, page, pageSize, userID, companyID uint) ([]OrderResponse, error)
	GetOrderCompanyInfo(ctx context.Context, userID, companyID uint) (*CompanyOrdersInfoQty, error)
}

type OrdersConstraint interface {
	Order | OrderUpdated
}

type OrderQueryParams struct {
	Query       string
	ID          string
	Status      string
	PaymentType string
}

type Order struct {
	CustomerID     uint  `json:"customer_id" validate:"required,number"`
	TotalAmount    int64 `json:"total_amount" validate:"required,number"`
	TotalDiscount  int64 `json:"total_discount"`
	subTotalAmount int64
	status         string
	Items          []Items    `json:"items" validate:"required,dive,required"`
	Payments       []Payments `json:"payments" validate:"required,dive,required"`
	CreatedAt      time.Time  `json:"created_at"`
	UpdatedAt      time.Time  `json:"updated_at"`
}

func (o *Order) SetSubTotalAmount() {
	o.subTotalAmount = o.TotalAmount - o.TotalDiscount
}

func (o *Order) GetSubTotalAmount() int64 {
	return o.subTotalAmount
}

func (o *Order) HandleNewOrderStatus() {
	o.status = "PAID"

	for _, p := range o.Payments {
		if p.PaymentType == "AWAITING_PAYMENT" {
			o.status = "PENDING"
			break
		}
	}
}

func (o *Order) GetStatus() string {
	return o.status
}

type OrderUpdated struct {
	TotalAmount    int64 `json:"total_amount" validate:"required,number"`
	TotalDiscount  int64 `json:"total_discount"`
	subTotalAmount int64
	Payments       []Payments `json:"payments" validate:"required,dive,required"`
	Status         string     `json:"status"`
}

func (o *OrderUpdated) SetSubTotalAmount() {
	o.subTotalAmount = o.TotalAmount - o.TotalDiscount
}

func (o *OrderUpdated) GetSubTotalAmount() int64 {
	return o.subTotalAmount
}

func (o *OrderUpdated) HandleNewOrderStatus() {
	o.Status = "PAID"

	for _, p := range o.Payments {
		if p.PaymentType == "AWAITING_PAYMENT" {
			o.Status = "PENDING"
			break
		}
	}
}

type Items struct {
	ProductID uuid.UUID `json:"product_id" validate:"required"`
	Quantity  uint      `json:"quantity" validate:"required,number"`
}

type Payments struct {
	ID                 uuid.UUID `json:"id,omitempty"`
	OrderID            uuid.UUID `json:"order_id"`
	PaymentType        string    `json:"payment_type" validate:"required"`
	InstallmentsAmount uint      `json:"installments_amount"  validate:"number"`
	Amount             uint      `json:"amount" validate:"required"`
	CreatedAt          time.Time `json:"created_at"`
	UpdatedAt          time.Time `json:"updated_at"`
}

type OrderResponse struct {
	ID             uuid.UUID     `json:"id,omitempty"`
	CustomerID     uint          `json:"customer_id"`
	CompanyID      uint          `json:"company_id"`
	TotalAmount    int64         `json:"total_amount"`
	TotalDiscount  int64         `json:"total_discount"`
	SubTotalAmount int64         `json:"sub_total_amount"`
	Status         string        `json:"status"`
	OrdesDetail    []OrdersItems `json:"orders_items"`
	Payments       []Payments    `json:"payments"`
	Customer       *Customer     `json:"customer"`
	CreatedAt      time.Time     `json:"created_at"`
	UpdatedAt      time.Time     `json:"updated_at"`
}

type OrdersItems struct {
	OrderID   uuid.UUID        `json:"order_id"`
	ProductID uuid.UUID        `json:"product_id"`
	Quantity  uint             `json:"quantity"`
	Product   *ProductResponse `json:"product"`
}

type CompanyOrdersInfoQty struct {
	OrdersMonthQty                     int64            `json:"orders_month_qty"`
	OrdersWeekQty                      int64            `json:"orders_week_qty"`
	OrdersTodayQty                     int64            `json:"orders_today_qty"`
	OrdersPaymentPendingQty            int64            `json:"orders_payment_pending_qty"`
	OrdersPaymentCreditCardQty         int64            `json:"orders_payment_credit_card_qty"`
	OrdersPaymentMoneyQty              int64            `json:"orders_payment_money_qty"`
	OrdersPaymentDebitCardQty          int64            `json:"orders_payment_debit_card_qty"`
	OrdersTotalAmountLastMonth         int64            `json:"orders_total_amount_last_month"`
	OrdersTotalAmountLastWeek          int64            `json:"orders_total_amount_last_week"`
	OrdersTotalAmountToday             int64            `json:"orders_total_amount_today"`
	OrdersTotalAmountPaymentCreditCard int64            `json:"orders_total_amount_payment_credit_card"`
	OrdersTotalAmountPaymentMoney      int64            `json:"orders_total_amount_payment_money"`
	OrdersTotalAmountPaymentDebitCard  int64            `json:"orders_total_amount_payment_debit_card"`
	OrdersTotalAmountPaymentPending    int64            `json:"orders_total_amount_payment_pending"`
	LastRegisteredOrders               []OrderResponse `json:"last_registered_orders"`
}

func OrderResponseFromDataStore(o []models.Order) []OrderResponse {
	var orders []OrderResponse

	for _, os := range o {
		orders = append(orders, *MapedOrder(os))
	}
	return orders
}

func MapedOrder(o models.Order) *OrderResponse {
	or := OrderResponse{
		ID:             o.ID,
		CustomerID:     o.CustomerID,
		CompanyID:      o.CompanyID,
		TotalAmount:    o.TotalAmount,
		TotalDiscount:  o.TotalDiscount,
		SubTotalAmount: o.SubTotalAmount,
		Status:         o.Status,
		CreatedAt:      o.CreatedAt,
		UpdatedAt:      o.UpdatedAt,
	}

	if o.OrderDetail != nil {
		or.OrdesDetail = AllOrdersDetail(o.OrderDetail)
	}

	if o.Customer != nil {
		or.Customer = CustomerResponseFromDataStore(*o.Customer)
	}

	if o.OrderPayments != nil {
		or.Payments = AllOrdersPayment(o.OrderPayments)
	}

	return &or
}

func AllOrdersDetail(o []models.OrdersItems) []OrdersItems {
	var ods []OrdersItems

	for _, oi := range o {
		ods = append(ods, OrdersItems{
			OrderID:   oi.OrderID,
			ProductID: oi.ProductID,
			Quantity:  oi.Quantity,
			Product: &ProductResponse{
				ID:          oi.Product.ID,
				Active:      oi.Product.Active,
				Name:        oi.Product.Name,
				Price:       oi.Product.Price,
				CompanyID:   oi.Product.CompanyID,
				Description: oi.Product.Description,
				SalePrice:   oi.Product.SalePrice,
				Stock:       ProductStockFromDataStore(*oi.Product.Stock),
			},
		})
	}

	return ods
}

func AllOrdersPayment(o []models.OrdersPayments) []Payments {
	var opt []Payments

	for _, pt := range o {
		opt = append(opt, Payments{
			ID:                 pt.ID,
			OrderID:            pt.OrderID,
			PaymentType:        pt.PaymentType,
			InstallmentsAmount: pt.InstallmentsAmount,
			Amount:             pt.Amount,
			CreatedAt:          pt.CreatedAt,
			UpdatedAt:          pt.UpdatedAt,
		})
	}

	return opt
}
