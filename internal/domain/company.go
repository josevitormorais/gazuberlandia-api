package domain

import (
	"context"
	"gazuberlandia/internal/datastore/postgres/models"
	"reflect"
	"time"
)

type CompaniesService interface {
	CreateCompany(ctx context.Context, userID uint, companie *CreateCompaniesRequest) error
	FindCompanyById(ctx context.Context, id uint, userID uint) (*CompaniesResponse, error)
	GetAllCompanyFromUserID(ctx context.Context, userID uint) (*[]CompaniesResponse, error)
}

type CompaniesResponse struct {
	ID          uint                      `json:"id"`
	UserID      uint                      `json:"user_id"`
	Email       string                    `json:"email"`
	Active      bool                      `json:"active"`
	Cnpj        string                    `json:"cnpj"`
	Phone       string                    `json:"phone"`
	FantasyName string                    `json:"fantasy_name"`
	Address     *CompaniesAddressResponse `json:"address"`
	CreatedAt   time.Time                 `json:"created_at"`
	UpdatedAt   time.Time                 `json:"updated_at"`
}

type CreateCompaniesRequest struct {
	UserID      uint   `json:"user_id"`
	Email       string `json:"email"`
	Active      bool   `json:"active"`
	FantasyName string `json:"fantasy_name"`
	Cnpj        string `json:"cnpj"`
	Phone       string `json:"Phone"`
	Address     struct {
		Street       string `json:"street"`
		Neighborhood string `json:"neighborhood"`
		City         string `json:"city"`
		State        string `json:"state"`
		Complement   string `json:"complement"`
		Number       uint   `json:"number"`
	}
}

type CompaniesAddressResponse struct {
	ID           uint      `json:"id"`
	Street       string    `json:"street"`
	Neighborhood string    `json:"neighborhood"`
	City         string    `json:"city"`
	State        string    `json:"state"`
	Complement   string    `json:"complement"`
	Number       uint      `json:"number"`
	CompanyID    uint      `json:"company_id"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}

func CompanyResponseFromDataStore(c models.Companie) *CompaniesResponse {
	cr := &CompaniesResponse{
		ID:          c.ID,
		UserID:      c.UserID,
		Email:       c.Email,
		Active:      c.Active,
		Cnpj:        c.Cnpj,
		Phone:       c.Phone,
		FantasyName: c.FantasyName,
		CreatedAt:   c.CreatedAt,
		UpdatedAt:   c.UpdatedAt,
	}

	if reflect.ValueOf(c.Address).IsNil() {
		return cr
	}

	cr.Address = &CompaniesAddressResponse{
		ID:           c.Address.ID,
		Street:       c.Address.Street,
		Neighborhood: c.Address.Neighborhood,
		City:         c.Address.City,
		State:        c.Address.State,
		Complement:   c.Address.Complement,
		Number:       c.Address.Number,
		CompanyID:    c.Address.CompanyID,
		CreatedAt:    c.CreatedAt,
		UpdatedAt:    c.UpdatedAt,
	}

	return cr
}

func AllCompanies(c []models.Companie) *[]CompaniesResponse {
	var cps []CompaniesResponse

	for _, cp := range c {
		cps = append(cps, *CompanyResponseFromDataStore(cp))
	}

	return &cps
}
