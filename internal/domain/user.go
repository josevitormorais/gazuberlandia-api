package domain

import (
	"context"
	"gazuberlandia/internal/datastore/postgres/models"
	"time"
)

type UserService interface {
	CreateUser(ctx context.Context, user *CreateUserRequest) (*UserResponse, error)
	CreateRegister(ctx context.Context, register *CreateRegister) error
	UpdateUser(ctx context.Context, userId int, user *UpdateUserRequest) error
	DeleteUser(ctx context.Context, userId int) error
	FindUserById(ctx context.Context, id int) (*UserResponse, error)
	FindUserByEmail(ctx context.Context, email string, withCP bool) (*UserResponse, error)
	GetUserLogged(ctx context.Context, userID int, cpAddress bool) (*UserResponse, error)
}

type LoginRequest struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}

type User struct {
	ID           uint      `json:"id,omitempty"`
	Active       bool      `json:"active"`
	Name         string    `json:"name"`
	Email        string    `json:"email"`
	Password     string    `json:"password"`
	Phone        string    `json:"phone"`
	Cpf          string    `json:"cpf"`
	IsWhatsapp   bool      `json:"is_whatsapp"`
	RegisterStep string    `json:"register_step"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}

type UserResponse struct {
	ID           uint                 `json:"id,omitempty"`
	Active       bool                 `json:"active"`
	Name         string               `json:"name"`
	Email        string               `json:"email"`
	Phone        string               `json:"phone"`
	Cpf          string               `json:"cpf"`
	IsWhatsapp   bool                 `json:"is_whatsapp"`
	RegisterStep string               `json:"register_step"`
	Companies    *[]CompaniesResponse `json:"companies"`
	CreatedAt    time.Time            `json:"created_at"`
	UpdatedAt    time.Time            `json:"updated_at"`
}

type CreateUserRequest struct {
	Name         string `json:"name"`
	Email        string `json:"email"`
	Active       bool   `json:"active"`
	Password     string `json:"password"`
	Phone        string `json:"phone"`
	Cpf          string `json:"cpf"`
	IsWhatsapp   bool   `json:"is_whatsapp"`
	RegisterStep string `json:"register_step"`
}

type UpdateUserRequest struct {
	Name         string `json:"name"`
	Email        string `json:"email"`
	Active       bool   `json:"active"`
	Phone        string `json:"phone"`
	Cpf          string `json:"cpf"`
	RegisterStep string `json:"register_step"`
	IsWhatsapp   bool   `json:"is_whatsapp"`
}

type CreateRegister struct {
	User
	Companie CreateCompaniesRequest
}

func UserResponseFromDataStore(user *models.User) *UserResponse {
	return &UserResponse{
		ID:           user.ID,
		Active:       user.Active,
		Name:         user.Name,
		Email:        user.Email,
		Phone:        user.Phone,
		Cpf:          user.Cpf,
		IsWhatsapp:   user.IsWhatsapp,
		RegisterStep: user.RegisterStep,
		CreatedAt:    user.CreatedAt,
		UpdatedAt:    user.UpdatedAt,
		Companies:    AllCompanies(user.Companies),
	}
}
