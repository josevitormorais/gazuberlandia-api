package domain

import (
	"context"
	"fmt"
	"gazuberlandia/internal/datastore/postgres/models"
	"mime/multipart"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
	"time"

	"github.com/gofrs/uuid"
)

type ProductService interface {
	CreateProduct(ctx context.Context, companyID uint, product *Product, f *multipart.FileHeader) error
	DeleteProduct(ctx context.Context, id uuid.UUID) error
	UpdateProduct(ctx context.Context, companyID uint, product *UpdateProduct, f *multipart.FileHeader) error
	FindProductById(ctx context.Context, id uuid.UUID, companyID uint) (*ProductResponse, error)
	GetAllProduct(ctx context.Context, companyID uint) (*[]ProductResponse, error)
	SearchProducts(ctx context.Context, companyID, userID, page, pageSize uint, query string) (*[]ProductResponse, error)
	SearchProductsPaginated(ctx context.Context, userID, companyID, page, pageSize uint) (*[]ProductResponse, error)
}

type Product struct {
	ID               uuid.UUID `json:"id" form:"id"`
	Active           bool      `json:"active" form:"active" validate:"boolean"`
	Name             string    `json:"name" form:"name"`
	Description      string    `json:"description" form:"description"`
	Quantity         int       `json:"quantity" form:"quantity"`
	Price            int64     `json:"price" form:"price"`
	SalePrice        int64     `json:"sale_price" form:"sale_price"`
	OriginalFileName string    `json:"original_file_name" form:"original_file_name"`
	fileName         string
	isNewProductFile bool
}

func (p *Product) SetNewFileName(name string) error {
	u, _ := uuid.NewV4()
	re, err := regexp.Compile(`\d|\W`)

	if err != nil {
		return err
	}

	p.fileName = fmt.Sprintf("%s-%s%s", re.ReplaceAllString(strings.Split(name, ".")[0], ""), u.String(), filepath.Ext(name))

	return nil
}

func (p *Product) GetFileName() string {
	return p.fileName
}

func (p *Product) IsNewProductFile() bool {
	return p.isNewProductFile
}

func (p *Product) SetIsNewProductFile() {
	p.isNewProductFile = true
}

type SearchProducts struct {
	ID          uuid.UUID `json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Price       int64     `json:"price"`
}

type UpdateProduct struct {
	Product
	Stock ProductsStock `json:"stock" form:"stock"`
}

type ProductResponse struct {
	ID               uuid.UUID      `json:"id"`
	CompanyID        uint           `json:"company_id"`
	Active           bool           `json:"active"`
	Name             string         `json:"name"`
	Description      string         `json:"description"`
	Price            int64          `json:"price"`
	Quantity         int            `json:"quantity,omitempty"`
	SalePrice        int64          `json:"sale_price"`
	Stock            *ProductsStock `json:"stock"`
	ImageUrl         string         `json:"image_url"`
	OriginalFileName string         `json:"original_file_name"`
	CreatedAt        time.Time      `json:"created_at"`
	UpdatedAt        time.Time      `json:"updated_at"`
}

type ProductsStock struct {
	ID        uint      `json:"id" form:"id"`
	ProductID uuid.UUID `json:"product_id" form:"product_id"`
	Quantity  int       `json:"quantity" form:"quantity"`
	CreatedAt time.Time `json:"created_at" form:"created_at"`
	UpdatedAt time.Time `json:"updated_at" form:"updated_at"`
}

func ProductResponseFromDataStore(p models.Product) *ProductResponse {
	//TODO dynamic bucket, env or define on database per company
	bucket := "gazuberlandia"
	pr := &ProductResponse{
		ID:               p.ID,
		CompanyID:        p.CompanyID,
		Active:           p.Active,
		Name:             p.Name,
		Description:      p.Description,
		Price:            p.Price,
		SalePrice:        p.SalePrice,
		ImageUrl:         fmt.Sprintf("https://storage.cloud.google.com/%s/%s", bucket, p.FileName), //TODO define storage url on env
		OriginalFileName: p.OriginalFileName,
		CreatedAt:        p.CreatedAt,
		UpdatedAt:        p.UpdatedAt,
	}

	if reflect.ValueOf(p.Stock).IsNil() {
		return pr
	}

	pr.Stock = ProductStockFromDataStore(*p.Stock)
	return pr
}

func ProductStockFromDataStore(p models.ProductsStock) *ProductsStock {
	return &ProductsStock{
		ID:        p.ID,
		ProductID: p.ProductID,
		Quantity:  p.Quantity,
		CreatedAt: p.CreatedAt,
		UpdatedAt: p.UpdatedAt,
	}
}

func AllProducts(p []models.Product) *[]ProductResponse {
	var pds []ProductResponse

	for _, pd := range p {
		pds = append(pds, *ProductResponseFromDataStore(pd))
	}

	return &pds
}
