package domain

import (
	"context"
	"errors"
	"time"

	"github.com/golang-jwt/jwt"
)

type Auth interface {
	Login(ctx context.Context, email, password string, jwtSecret string) (string, error)
}

type JwtCustomClaims struct {
	UserID uint `json:"user_id"`
	jwt.StandardClaims
}

func GenerateJWT(userID uint, jwtSecret string) (string, error) {
	claims := &JwtCustomClaims{
		UserID: userID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 24 * 30).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tk, err := token.SignedString([]byte(jwtSecret))
	if err != nil {
		return "", errors.New("internal server error")
	}

	return tk, nil
}
