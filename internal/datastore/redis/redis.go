package redis

import (
	"fmt"
)

type Config struct {
	User     string
	Password string
	DB       string
	Dsn      string
	Addr     string
}

func (r *Config) Url() string {
	return fmt.Sprintf("redis://%s:%s@%s:%s/%s", r.User, r.Password, r.Dsn, r.Addr, r.DB)
}

// func (r *Config) Options() *redis.Options {
// 	return &redis.Options{
// 		Addr:     "localhost:" + r.Addr,
// 		Password: "",
// 		DB:       0,
// 	}
// }

// func (r *Config) Open(o *redis.Options) *redis.Client {
// 	log.Println("[REDIS] Connection initialized")
// 	return redis.NewClient(o)
// }
