CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE EXTENSION unaccent;

-- CREATE
-- OR REPLACE FUNCTION uuid_or_null(str text) RETURNS uuid AS $ $ BEGIN RETURN str :: uuid;
-- EXCEPTION
-- WHEN invalid_text_representation THEN RETURN NULL;
-- END;
-- $ $ LANGUAGE plpgsql;
-- Mover os Fields  TEXT
CREATE TABLE users (
    id SERIAL NOT NULL PRIMARY KEY,
    active boolean NOT NULL DEFAULT true,
    name TEXT,
    email TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL,
    phone TEXT NOT NULL UNIQUE,
    cpf TEXT NOT NULL UNIQUE,
    is_whatsapp boolean not null default false,
    register_step text,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE companies (
    id SERIAL NOT NULL PRIMARY KEY,
    active boolean not null default true,
    email TEXT NOT NULL UNIQUE,
    cnpj TEXT NOT NULL UNIQUE,
    phone TEXT NOT NULL UNIQUE,
    fantasy_name TEXT NOT NULL,
    user_id INTEGER NOT NULL CONSTRAINT fk_users_to_companies REFERENCES users (id),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE companies_addresses (
    id SERIAL NOT NULL PRIMARY KEY,
    street TEXT NOT NULL,
    neighborhood TEXT NOT NULL,
    city TEXT NOT NULL,
    state TEXT NOT NULL,
    number INTEGER NOT NULL,
    complement TEXT,
    company_id INTEGER NOT NULL UNIQUE CONSTRAINT fk_companies_to_companies_address REFERENCES companies (id),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE customers (
    id SERIAL NOT NULL PRIMARY KEY,
    company_id INTEGER NOT NULL CONSTRAINT fk_companies_to_customers REFERENCES companies (id),
    active boolean not null default true,
    name TEXT,
    email TEXT UNIQUE,
    phone TEXT UNIQUE,
    cpf TEXT UNIQUE,
    is_whatsapp boolean not null default true,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE products (
    id uuid DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    active boolean not null default true,
    name TEXT NOT NULL,
    description TEXT,
    price INTEGER NOT NULL,
    company_id INTEGER NOT NULL CONSTRAINT fk_companies_to_products REFERENCES companies (id),
    -- unitary_price INTEGER NOT NULL,
    sale_price INTEGER NOT NULL,
    file_name TEXT NOT NULL,
    original_file_name TEXT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE customers_addresses (
    id SERIAL NOT NULL PRIMARY KEY,
    street TEXT NOT NULL,
    neighborhood TEXT NOT NULL,
    city TEXT NOT NULL,
    state TEXT NOT NULL,
    number INTEGER NOT NULL,
    complement TEXT,
    customer_id INTEGER NOT NULL CONSTRAINT fk_customers_to_customer_address REFERENCES customers (id),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE orders (
    id uuid DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    customer_id INTEGER NOT NULL CONSTRAINT fk_customers_to_orders REFERENCES customers (id),
    company_id INTEGER NOT NULL CONSTRAINT fk_companies_to_orders REFERENCES companies (id),
    total_amount INTEGER NOT NULL,
    total_discount INTEGER NOT NULL,
    sub_total_amount INTEGER NOT NULL,
    -- installments_amount INTEGER NOT NULL,
    -- payment_type TEXT NOT NULL,
    status TEXT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE orders_items (
    id SERIAL NOT NULL PRIMARY KEY,
    order_id uuid NOT NULL CONSTRAINT fk_orders_to_orders_items REFERENCES orders (id),
    product_id uuid NOT NULL CONSTRAINT fk_products_to_orders_items REFERENCES products (id),
    quantity INTEGER NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE orders_status (
    id SERIAL NOT NULL PRIMARY KEY,
    status TEXT NOT NULL,
    order_id uuid NOT NULL CONSTRAINT fk_orders_to_orders_status REFERENCES orders (id),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE products_stocks (
    id SERIAL NOT NULL PRIMARY KEY,
    product_id uuid NOT NULL UNIQUE CONSTRAINT fk_products_to_products_stock REFERENCES products (id),
    quantity INTEGER NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE orders_payments (
    id uuid DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    order_id uuid NOT NULL CONSTRAINT fk_orders_to_orders_payments REFERENCES orders (id),
    payment_type TEXT NOT NULL,
    installments_amount INTEGER NOT NULL,
    amount INTEGER NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);