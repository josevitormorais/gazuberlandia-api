package postgres

import (
	"context"
	"gazuberlandia/internal/datastore/postgres/models"
	"gazuberlandia/internal/domain"
	"strings"

	"github.com/gofrs/uuid"
	"github.com/uptrace/bun"
)

func (p *postgres) CreateOrder(ctx context.Context, order *domain.Order, companyID uint) error {
	p.DB.RegisterModel((*models.OrdersItems)(nil))
	log := domain.Logger(p.logger, ctx)

	return p.DB.RunInTx(ctx, nil, func(ctx context.Context, tx bun.Tx) error {

		o := models.Order{
			CustomerID:     order.CustomerID,
			CompanyID:      companyID,
			TotalAmount:    order.TotalAmount,
			TotalDiscount:  order.TotalDiscount,
			SubTotalAmount: order.GetSubTotalAmount(),
			Status:         order.GetStatus(),
		}

		_, err := tx.NewInsert().Model(&o).Exec(ctx, &o)

		if err := p.handleQueryError(err); err != nil {
			log.Error().Err(err).Msg("error trying when create a new order on database.")
			return err
		}

		var orderItems []models.OrdersItems
		var productsID []uuid.UUID

		for _, item := range order.Items {

			orderItems = append(orderItems, models.OrdersItems{
				OrderID:   o.ID,
				ProductID: item.ProductID,
				Quantity:  item.Quantity,
			})

			productsID = append(productsID, item.ProductID)

		}

		_, err = tx.NewInsert().Model(&orderItems).Exec(ctx)

		if err := p.handleQueryError(err); err != nil {
			log.Error().Err(err).Msg("error trying when create items for order on database.")
			return err
		}

		var oPayments []models.OrdersPayments

		for _, pt := range order.Payments {

			oPayments = append(oPayments, models.OrdersPayments{
				OrderID:            o.ID,
				PaymentType:        pt.PaymentType,
				InstallmentsAmount: pt.InstallmentsAmount,
				Amount:             pt.Amount,
			})

		}
		_, err = tx.NewInsert().Model(&oPayments).Exec(ctx)

		if err := p.handleQueryError(err); err != nil {
			log.Error().Err(err).Msg("error trying when create payments for order in database.")
			return err
		}
		var prdsStock []models.ProductsStock

		err = tx.NewSelect().
			Column("*").
			Model(&prdsStock).
			Where("product_id IN (?)", bun.In(productsID)).
			Scan(ctx)

		if err != nil {
			log.Error().Err(err).Msg("error when trying find products to update sotcks in database.")
			return err
		}

		var newPrdsStock []models.ProductsStock

		for _, prd := range prdsStock {
			for _, oi := range orderItems {
				if strings.Compare(oi.ProductID.String(), prd.ProductID.String()) != 0 {
					continue
				}
				newPrdsStock = append(newPrdsStock, models.ProductsStock{
					ID:        prd.ID,
					ProductID: prd.ProductID,
					Quantity:  prd.Quantity - int(oi.Quantity),
				})
			}
		}

		_, err = tx.NewUpdate().
			Model(&newPrdsStock).
			Column("quantity").
			Bulk().
			Exec(ctx)

		if err := p.handleQueryError(err); err != nil {
			log.Error().Err(err).Msg("error when trying to update product sotck in database.")
			return err
		}

		return nil
	})

}

func (p *postgres) UpdateOrder(ctx context.Context, order *domain.OrderUpdated, orderID uuid.UUID, userID, companyID uint) error {
	log := domain.Logger(p.logger, ctx)

	o := models.Order{
		ID:             orderID,
		CompanyID:      companyID,
		TotalAmount:    order.TotalAmount,
		TotalDiscount:  order.TotalDiscount,
		SubTotalAmount: order.GetSubTotalAmount(),
		Status:         order.Status,
	}

	op := []models.OrdersPayments{}

	return p.DB.RunInTx(ctx, nil, func(ctx context.Context, tx bun.Tx) error {
		_, err := tx.NewUpdate().
			Model(&o).
			WherePK().
			Where("company_id = ?", companyID).
			Column("total_amount", "total_discount", "sub_total_amount", "status", "updated_at").
			Exec(ctx, &o)

		if err := p.handleQueryError(err); err != nil {
			log.Error().Err(err).Msg("error when trying to update the order in the database.")
			return err
		}

		_, err = tx.NewSelect().
			Model(&op).
			Column("payment_type", "installments_amount", "amount").
			Where("order_id = ?", orderID).
			Exec(ctx, &op)

		if err := p.handleQueryError(err); err != nil {
			log.Error().Err(err).Str("order_id", orderID.String()).Msg("error when trying to select old payments for order from database.")
			return err
		}

		shouldUpdatePayments := false
		countEquals := 0

		for idx, payOnOder := range order.Payments {
			if len(order.Payments) != len(op) {
				break
			}

			for _, payOnDatabase := range op {
				if payOnDatabase.Amount == payOnOder.Amount && payOnDatabase.InstallmentsAmount == payOnOder.InstallmentsAmount && payOnDatabase.PaymentType == payOnOder.PaymentType {
					countEquals++
				}
			}

			if idx+1 >= len(order.Payments) && countEquals != len(order.Payments) {
				shouldUpdatePayments = true
			}
		}

		if !shouldUpdatePayments {
			return nil
		}

		var oPayments []models.OrdersPayments

		for _, pt := range order.Payments {
			oPayments = append(oPayments, models.OrdersPayments{
				OrderID:            o.ID,
				PaymentType:        pt.PaymentType,
				InstallmentsAmount: pt.InstallmentsAmount,
				Amount:             pt.Amount,
			})
		}

		_, err = tx.NewDelete().Model(&models.OrdersPayments{}).Where("order_id = ?", orderID).Exec(ctx)

		if err := p.handleQueryError(err); err != nil {

			log.Error().Err(err).Str("order_id", orderID.String()).Msg("error when trying to delete old payments for order in database.")

			return err
		}

		_, err = tx.NewInsert().Model(&oPayments).Exec(ctx)

		if err := p.handleQueryError(err); err != nil {

			log.Error().Err(err).Str("order_id", orderID.String()).Msg("error when trying to insert the new payments for order in database.")

			return err
		}
		return nil
	})

}

func (p *postgres) GetAllOrder(ctx context.Context, userID, companyID uint) ([]models.Order, error) {
	p.DB.RegisterModel((*models.OrdersItems)(nil))

	o := []models.Order{}

	err := p.DB.NewSelect().
		Model(&o).
		Join(`INNER JOIN companies cp ON cp.id = o.company_id`).
		Join(`INNER JOIN users u ON u.id = cp.user_id`).
		Where("u.id = ?", &userID).
		Where("cp.id = ?", &companyID).
		Relation("OrderPayments").
		Relation("OrderDetail").
		Relation("OrderDetail.Product").
		Relation("OrderDetail.Product.Stock").
		Relation("Customer").
		Relation("Customer.Address").
		Order("created_at DESC").
		Scan(ctx)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error when trying get all orders in database.")
		return nil, err
	}
	return o, nil
}

func (p *postgres) FindOrderByID(ctx context.Context, orderID uuid.UUID, userID, companyID uint) (*models.Order, error) {
	o := models.Order{
		ID: orderID,
	}

	err := p.DB.NewSelect().
		Model(&o).
		Join(`INNER JOIN companies cp ON cp.id = o.company_id`).
		Join(`INNER JOIN users u ON u.id = cp.user_id`).
		Where("u.id = ?", &userID).
		Where("cp.id = ?", &companyID).
		Relation("OrderPayments").
		Relation("OrderDetail.Product").
		Relation("OrderDetail.Product.Stock").
		Relation("OrderDetail").
		Relation("Customer").
		Relation("Customer.Address").
		WherePK().
		Scan(ctx)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Str("order_id", orderID.String()).Msg("error when trying to find order by id in database.")
		return nil, err
	}
	return &o, nil
}

func (p *postgres) SearchOrderPaginated(ctx context.Context, page, pageSize, userID, companyID uint) ([]models.Order, error) {
	o := []models.Order{}

	paginate := p.Paginate(page, pageSize)

	err := p.DB.NewSelect().
		Model(&o).
		Join(`INNER JOIN companies cp ON cp.id = o.company_id`).
		Join(`INNER JOIN users u ON u.id = cp.user_id`).
		Where("u.id = ?", &userID).
		Where("cp.id = ?", &companyID).
		Relation("OrderPayments").
		Relation("OrderDetail.Product").
		Relation("OrderDetail.Product.Stock").
		Relation("OrderDetail").
		Relation("Customer").
		Relation("Customer.Address").
		Limit(paginate["limit"]).
		Offset(paginate["offset"]).
		Order("created_at DESC").
		Scan(ctx)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error when trying to search order paginated in database.")
		return nil, err
	}
	return o, nil
}

func (p *postgres) SearchOrder(ctx context.Context, q *domain.OrderQueryParams, page, pageSize, userID, companyID uint) ([]models.Order, error) {
	o := []models.Order{}

	paginate := p.Paginate(page, pageSize)

	err := p.DB.NewSelect().
		Model(&o).
		Join(`INNER JOIN companies cp ON cp.id = o.company_id`).
		Join(`INNER JOIN users u ON u.id = cp.user_id`).
		Where("u.id = ?", &userID).
		Where("cp.id = ?", &companyID).
		WhereGroup(" AND ", func(sq *bun.SelectQuery) *bun.SelectQuery {
			if q.PaymentType != "" {
				sq.Where("o.id IN (?)", sq.NewSelect().Column("op.order_id").Model(&models.OrdersPayments{}).Where(`op.payment_type = ?`, q.PaymentType))

			}
			if q.ID != "" {
				sq.Where(`o.id::text ILIKE concat('%',?,'%')`, q.ID)

			}
			if q.Status != "" {
				sq.Where(`o.status = ?`, q.Status)

			}

			return sq
		}).
		WhereGroup(" AND ", func(sq *bun.SelectQuery) *bun.SelectQuery {
			if q.Query != "" {
				sq.Where(`customer.cpf ILIKE concat('%',?,'%')`, q.Query).
					WhereOr(`customer.phone ILIKE concat('%',?,'%')`, q.Query).
					WhereOr(`lower(regexp_replace(unaccent(customer.name), '\W+', '', 'g')) ILIKE concat('%',lower(regexp_replace(unaccent(?), '\W+', '', 'g')),'%')`, q.Query).
					WhereOr(`lower(regexp_replace(unaccent(customer__address.street), '\W+', '', 'g')) ILIKE concat('%',lower(regexp_replace(unaccent(?), '\W+', '', 'g')),'%')`, q.Query)

			}
			return sq
		}).
		Relation("OrderPayments").
		Relation("OrderDetail.Product").
		Relation("OrderDetail.Product.Stock").
		Relation("OrderDetail").
		Relation("Customer").
		Relation("Customer.Address").
		Limit(paginate["limit"]).
		Offset(paginate["offset"]).
		Order("created_at DESC").
		Scan(ctx)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error when trying to search order in database.")
		return nil, err
	}

	return o, nil
}

func (p *postgres) GetOrderCompanyInfo(ctx context.Context, userID, companyID uint) (*models.CompanyOrdersInfoQty, error) {

	qInfo := `
		WITH payments as (
			SELECT
				op.order_id,
				op.payment_type,
				op.amount,
				op.created_at
			FROM
				orders_payments op
			INNER JOIN orders o ON o.id = op.order_id
				AND o.company_id = ?
			WHERE op.deleted_at ISNULL
			AND op.created_at::date >= now()::date - interval '30 day'
			GROUP BY
				op.order_id,
				op.payment_type,
				op.created_at,
				op.amount
		)
		SELECT
			count(1) filter (
				WHERE
					o.created_at::date >= now() - interval '30 day'
			) AS orders_month_qty,
			
			count(1) filter (
				WHERE
					o.created_at::date >= now() - interval '7 day'
			) AS orders_week_qty,
			
			count(1) filter (
				WHERE
					o.created_at::date >= now() - interval '24 hours'
			) AS orders_today_qty,

			coalesce(
				SUM(o.total_amount) filter (
					WHERE
						o.created_at::date >= now() - interval '30 day'
				),
				0
			) AS orders_total_amount_last_month,
			
			coalesce(
				SUM(o.total_amount) filter (
					WHERE
						o.created_at::date >= now() - interval '7 day'
				),
				0
			) AS orders_total_amount_last_week,
			
			coalesce(
				SUM(o.total_amount) filter (
					WHERE
						o.created_at::date >= now() - interval '24 hours'
				),
				0
			) AS orders_total_amount_today,

			coalesce(
				(
					SELECT
						SUM(p.amount)
					FROM
						payments p
					WHERE
						p.payment_type = 'AWAITING_PAYMENT'
				),
				0
			) AS orders_total_amount_payment_pending,

			count(1) filter (
				WHERE
					o.status = 'PENDING'
			) AS orders_payment_pending_qty,
			
			count(1) filter (
				WHERE
					o.id in (
						SELECT
							p.order_id
						FROM
							payments p
						WHERE
							p.payment_type = 'CREDIT_CARD'
					)
			) AS orders_payment_credit_card_qty,
			
			count(1) filter (
				WHERE
					o.id in (
						SELECT
							p.order_id
						FROM
							payments p
						WHERE
							p.payment_type = 'MONEY'
					)
			) AS orders_payment_money_qty,
			
			count(1) filter (
				WHERE
					o.id in (
						SELECT
							p.order_id
						FROM
							payments p
						WHERE
							p.payment_type = 'DEBIT_CARD'
					)
			) AS orders_payment_debit_card_qty,
			
			coalesce(
				(
					SELECT
						SUM(p.amount)
					FROM
						payments p
					WHERE
						p.payment_type = 'CREDIT_CARD'
				),
				0
			) AS orders_total_amount_payment_credit_card,
			
			coalesce(
				(
					SELECT
						SUM(p.amount)
					FROM
						payments p
					WHERE
						p.payment_type = 'MONEY'
				),
				0
			) AS orders_total_amount_payment_money,
			
			coalesce(
				(
					SELECT
						SUM(p.amount)
					FROM
						payments p
					WHERE
						p.payment_type = 'DEBIT_CARD'
				),
				0
			) AS orders_total_amount_payment_debit_card
		FROM
			orders o
		WHERE o.company_id = ?
		AND o.deleted_at IS NULL;
	`

	log := domain.Logger(p.logger, ctx)

	row, err := p.DB.QueryContext(ctx, qInfo, companyID, companyID)

	if err := p.handleQueryError(err); err != nil {
		log.Error().Err(err).Msg("error when trying infos to companie in database.")
		return nil, err
	}

	cpInfo := &models.CompanyOrdersInfoQty{}

	err = p.DB.ScanRows(ctx, row, cpInfo)

	if err := p.handleQueryError(err); err != nil {
		log.Error().Err(err).Msg("error when trying scan rows of query from database.")
		return nil, err
	}

	subQ := p.DB.NewSelect().
		Model((*models.Order)(nil)).
		Where("o.company_id = ?", &companyID).
		Relation("Customer").
		Order("o.created_at DESC").
		Limit(5)

	err = p.DB.NewSelect().
		TableExpr("(?) AS last_registred_orders", subQ).
		Scan(ctx, &cpInfo.LastRegisteredOrders)

	if err := p.handleQueryError(err); err != nil {
		log.Error().Err(err).Msg("error when trying to get last orders registered in database.")
		return nil, err
	}

	return cpInfo, nil
}

/* Anotation:
quando precisar fazer um select com soma e a tabela possuir dados duplicados
	por exemplo um id:
pode ser feito um select com somas e usar group by, para fazer a separação.
exemplo:
	SELECT
		op.order_id,
		op.payment_type,
		sum(op.amount) as amount_by_payment_type,
		op.created_at
	FROM orders_payments op
	where op.deleted_at is null
	GROUP BY op.order_id, op.payment_type, op.created_at
nesse caso o
	order_id, payment_type
pode existir varios duplicados.
*/

// qOrder := `SELECT *
// 			FROM (
// 				SELECT
// 					o.*
// 				FROM
// 					orders o
// 				INNER JOIN customers ct on ct.id = o.customer_id
// 				WHERE o.company_id = ?
// 				ORDER BY o.created_at DESC LIMIT 5
// 			) AS last_registred_orders
// 	ORDER BY last_registred_orders.created_at ASC;`

// rows, err := p.DB.QueryContext(ctx, qOrder, companyID)

// err = p.DB.ScanRows(ctx, rows, &cpInfo.LastRegisteredOrders)
// if err := p.handleQueryError(err); err != nil {
// 	return nil, err
// }
