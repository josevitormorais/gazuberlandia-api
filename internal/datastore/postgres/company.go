package postgres

import (
	"context"
	"gazuberlandia/internal/datastore/postgres/models"
	"gazuberlandia/internal/domain"

	"github.com/uptrace/bun"
)

func (p *postgres) CreateCompany(ctx context.Context, userID uint, companie *domain.CreateCompaniesRequest) error {

	err := p.DB.RunInTx(ctx, nil, func(ctx context.Context, tx bun.Tx) error {
		return CreateCompanie(ctx, p, &tx, userID, companie)
	})

	return err
}

func CreateCompanie(ctx context.Context, p *postgres, tx *bun.Tx, userID uint, companie *domain.CreateCompaniesRequest) error {
	log := domain.Logger(p.logger, ctx)

	c := &models.Companie{
		UserID:      userID,
		Email:       companie.Email,
		Active:      (!companie.Active),
		Cnpj:        companie.Cnpj,
		Phone:       companie.Phone,
		FantasyName: companie.FantasyName,
	}

	if _, err := tx.NewInsert().Model(c).Exec(ctx); p.handleQueryError(err) != nil {
		log.Error().Err(err).Msg("error trying when create a new company on database.")
		return err
	}

	ca := &models.CompaniesAddresses{
		Street:       companie.Address.Street,
		Neighborhood: companie.Address.Neighborhood,
		City:         companie.Address.City,
		State:        companie.Address.State,
		Complement:   companie.Address.Complement,
		Number:       companie.Address.Number,
		CompanyID:    c.ID,
	}

	if _, err := tx.NewInsert().Model(ca).Exec(ctx); p.handleQueryError(err) != nil {
		log.Error().Err(err).Msg("error trying when create a a address for company on database.")
		return err
	}

	return nil
}

func (p *postgres) FindCompanyById(ctx context.Context, id uint, userID uint) (*models.Companie, error) {
	companie := models.Companie{
		ID: id,
	}

	err := p.DB.NewSelect().
		Model(&companie).
		Relation("Address").
		Where("cp.user_id = ?", userID).
		WherePK().
		Scan(ctx, &companie)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error trying to find company by id.")
		return nil, err
	}
	return &companie, nil
}

func (p *postgres) GetAllCompanyFromUserID(ctx context.Context, userID uint) (*[]models.Companie, error) {
	companie := []models.Companie{}

	err := p.DB.NewSelect().Model(&companie).Relation("Address").
		Where("user_id = ?", &userID).
		Order("created_at").
		Scan(ctx, &companie)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error trying get all company.")
		return nil, err
	}
	return &companie, nil
}
