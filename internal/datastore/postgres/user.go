package postgres

import (
	"context"
	"gazuberlandia/internal/datastore/postgres/models"
	"gazuberlandia/internal/domain"

	_ "github.com/jackc/pgx/v4"
	"github.com/uptrace/bun"
)

func (p *postgres) CreateUser(ctx context.Context, user *domain.CreateUserRequest) (*models.User, error) {
	u := models.User{
		Active:       user.Active,
		Name:         user.Name,
		Password:     user.Password,
		Email:        user.Email,
		Phone:        user.Phone,
		Cpf:          user.Cpf,
		IsWhatsapp:   user.IsWhatsapp,
		RegisterStep: user.RegisterStep,
	}

	if _, err := p.DB.NewInsert().Model(&u).Exec(ctx); err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error trying to insert user on database.")
		return nil, err
	}

	return &u, nil
}

func (p *postgres) CreateRegister(ctx context.Context, register *domain.CreateRegister) error {

	err := p.DB.RunInTx(ctx, nil, func(ctx context.Context, tx bun.Tx) error {
		u := models.User{
			Active:       register.Active,
			Name:         register.Name,
			Password:     register.Password,
			Email:        register.Email,
			Phone:        register.Phone,
			Cpf:          register.Cpf,
			IsWhatsapp:   register.IsWhatsapp,
			RegisterStep: register.RegisterStep,
		}

		if _, err := tx.NewInsert().Model(&u).Exec(ctx); p.handleQueryError(err) != nil {
			domain.Logger(p.logger, ctx).Error().Err(err).Msg("error trying to create a new register on database.")
			return err
		}

		return CreateCompanie(ctx, p, &tx, u.ID, &register.Companie)
	})

	return err
}

func (p *postgres) UpdateUser(ctx context.Context, userId int, user *domain.UpdateUserRequest) error {
	u := &models.User{
		ID:           uint(userId),
		Name:         user.Name,
		Email:        user.Email,
		Active:       user.Active,
		Phone:        user.Phone,
		Cpf:          user.Cpf,
		IsWhatsapp:   user.IsWhatsapp,
		RegisterStep: user.RegisterStep,
	}
	_, err := p.DB.NewUpdate().Model(u).OmitZero().WherePK().Exec(ctx)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error trying to update user on database.")
		return err
	}
	return nil
}

func (p *postgres) DeleteUser(ctx context.Context, userId int) error {
	_, err := p.DB.NewDelete().Model(&models.User{ID: uint(userId)}).WherePK().Exec(ctx)
	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error trying delete user on database.")
		return err
	}

	return nil
}

func (p *postgres) FindUserById(ctx context.Context, id int) (*models.User, error) {
	user := models.User{
		ID: uint(id),
	}

	err := p.DB.NewSelect().Model(&user).Relation("Companies").WherePK().Scan(ctx, &user)
	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error find user by id on database.")
		return nil, err
	}
	return &user, nil
}

func (p *postgres) FindUserByEmail(ctx context.Context, email string, withCP bool) (*models.User, error) {
	log := domain.Logger(p.logger, ctx)
	user := models.User{}

	if withCP {
		err := p.DB.NewSelect().
			Model(&user).
			Relation("Companies").
			Where("u.email = ?", email).
			Scan(ctx)

		if err != nil {
			log.Error().Err(err).Msg("error find user with companie by email on database.")
			return nil, err
		}

		return &user, nil
	}

	err := p.DB.NewSelect().Model(&user).Where("email = ?", email).Scan(ctx, &user)
	if err != nil {
		log.Error().Err(err).Msg("error find user by email on database.")
		return nil, err
	}
	return &user, nil
}

func (p *postgres) GetUserLogged(ctx context.Context, userID int, cpAddress bool) (*models.User, error) {
	user := models.User{
		ID: uint(userID),
	}

	query := p.DB.NewSelect().Model(&user).Relation("Companies")

	if cpAddress {
		query.Relation("Companies.Address")
	}

	err := query.WherePK().Scan(ctx)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error trying get logged user on databse.")
		return nil, err
	}

	return &user, nil
}
