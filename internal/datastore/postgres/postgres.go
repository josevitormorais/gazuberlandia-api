package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"gazuberlandia/internal/config"
	"os"

	"github.com/golang-migrate/migrate/v4"
	migratePgDriver "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"
	"github.com/oiime/logrusbun"
	"github.com/rs/zerolog"
	"github.com/sirupsen/logrus"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
)

type postgres struct {
	DB *bun.DB
	logger *zerolog.Logger
}

func NewPostgres(ctx context.Context, url string, logger *zerolog.Logger) (*postgres, error) {

	log := logger.With().Str("DATABASE", "[POSTGRES]").Logger()

	cfg, err := pgx.ParseConfig(url)
	if err != nil {
		log.Error().Err(err).Msg("Error to parsing url connection")
		return nil, err
	}

	cfg.PreferSimpleProtocol = true
	pgxDB := stdlib.OpenDB(*cfg)

	db := bun.NewDB(pgxDB, pgdialect.New())

	if os.Getenv("ENVIRONMENT") != "Prod" {
		logDB := logrus.New()
		logDB.SetFormatter(&logrus.JSONFormatter{
			PrettyPrint: true,
		})
		db.AddQueryHook(logrusbun.NewQueryHook(logrusbun.QueryHookOptions{
			Logger:     logDB,
			QueryLevel: logrus.InfoLevel,
			ErrorLevel: logrus.InfoLevel,
		}))
	}

	if err := db.Ping(); err != nil {
		log.Error().Err(err).Msg("Error to connect database.")
		return nil, err
	}

	log.Info().Msg("Connection initialized")
	return &postgres{DB: db, logger: &log}, nil
}

func (p *postgres) Close() {
	fmt.Println("close database")
	p.DB.Close()
}

func (p *postgres) RunMigrations(filePath string, logger *zerolog.Logger, conf *config.Config) error {
	driver, err := migratePgDriver.WithInstance(p.DB.DB, &migratePgDriver.Config{})
	if err != nil {
		logger.Error().Err(err).Msg("[MIGRATION] Error database instance")
		return err
	}

	m, err := migrate.NewWithDatabaseInstance(filePath, conf.PGdb, driver)
	if err != nil {
		logger.Error().Err(err).Msg("[MIGRATION] Error when trying to run migrations. Reason: ")
		return err
	}
	return m.Up()
}

func (p *postgres) Paginate(page, pageSize uint) map[string]int {
	return map[string]int{
		"limit":  int(pageSize),
		"offset": int(pageSize) * (int(page) - 1),
	}
}

func (p *postgres) handleQueryError(err error) error {
		if err != nil && err != sql.ErrNoRows {
		return err
	}
	return nil
}
