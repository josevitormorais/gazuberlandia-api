package models

import (
	"time"

	"github.com/uptrace/bun"
)

type User struct {
	bun.BaseModel `bun:",alias:u"`
	ID            uint `bun:",pk,default,autoincrement"`
	Active        bool
	Name          string
	Password      string
	Email         string
	Phone         string
	Cpf           string
	IsWhatsapp    bool
	RegisterStep  string
	Companies     []Companie `bun:"rel:has-many,join:id=user_id"`
	CreatedAt     time.Time  `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt     time.Time  `bun:",nullzero,notnull,default:current_timestamp"`
	DeletedAt     time.Time  `bun:",soft_delete,nullzero"`
}
