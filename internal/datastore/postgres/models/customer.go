package models

import (
	"context"
	"time"

	"github.com/uptrace/bun"
)

type Customer struct {
	bun.BaseModel   `bun:",alias:ct"`
	ID              uint `bun:",pk,autoincrement"`
	Name            string
	Email           string `bun:",nullzero"`
	Active          bool
	Phone           string `bun:",nullzero"`
	Cpf             string `bun:",nullzero"`
	CompanyID       uint
	IsWhatsapp      bool
	AmountPurchased int                 `bun:",scanonly"`
	PurchasedQty    int                 `bun:",scanonly"`
	Address         *CustomersAddresses `bun:",rel:has-one,join:id=customer_id"`
	CreatedAt       time.Time           `bun:",notnull,default:current_timestamp"`
	UpdatedAt       time.Time           `bun:",notnull,default:current_timestamp"`
	DeletedAt       time.Time           `bun:",soft_delete,nullzero"`
}

type CustomersAddresses struct {
	bun.BaseModel `bun:",alias:ca"`
	ID            uint `bun:",pk,autoincrement"`
	Street        string
	Neighborhood  string
	City          string
	State         string
	Complement    string
	Number        uint
	CustomerID    uint
	CreatedAt     time.Time `bun:",notnull,default:current_timestamp"`
	UpdatedAt     time.Time `bun:",notnull,default:current_timestamp"`
	DeletedAt     time.Time `bun:",soft_delete,nullzero"`
}

var _ bun.BeforeAppendModelHook = (*CustomersAddresses)(nil)

func (c *CustomersAddresses) BeforeAppendModel(ctx context.Context, query bun.Query) error {
	switch query.(type) {
	case *bun.InsertQuery:
		c.CreatedAt = time.Now()
		c.UpdatedAt = time.Now()
	case *bun.UpdateQuery:
		c.UpdatedAt = time.Now()
	}
	return nil
}

var _ bun.BeforeAppendModelHook = (*Customer)(nil)

func (c *Customer) BeforeAppendModel(ctx context.Context, query bun.Query) error {
	switch query.(type) {
	case *bun.InsertQuery:
		c.CreatedAt = time.Now()
		c.UpdatedAt = time.Now()
	case *bun.UpdateQuery:
		c.UpdatedAt = time.Now()
	}
	return nil
}
