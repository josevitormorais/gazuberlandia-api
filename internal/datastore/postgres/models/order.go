package models

import (
	"context"
	"time"

	"github.com/gofrs/uuid"
	"github.com/uptrace/bun"
)

type Order struct {
	bun.BaseModel  `bun:",alias:o"`
	ID             uuid.UUID `bun:"id,pk,type:uuid"`
	CustomerID     uint
	CompanyID      uint
	Status         string
	TotalAmount    int64
	TotalDiscount  int64
	SubTotalAmount int64
	OrderDetail    []OrdersItems    `bun:"rel:has-many"`
	OrderPayments  []OrdersPayments `bun:"rel:has-many"`
	Customer       *Customer        `bun:"rel:belongs-to,join:customer_id=id"`
	CreatedAt      time.Time        `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt      time.Time        `bun:",nullzero,notnull,default:current_timestamp"`
	DeletedAt      time.Time        `bun:",soft_delete,nullzero"`
}

// 1 Pedido => (N) Items => (N) produtos

type OrdersItems struct {
	bun.BaseModel `bun:",alias:oi"`
	ID            int `bun:"id,pk,autoincrement"`
	Quantity      uint
	OrderID       uuid.UUID `bun:",type:uuid"`
	ProductID     uuid.UUID `bun:",type:uuid"`
	Product       *Product  `bun:"rel:belongs-to,join:product_id=id"`
}

type OrdersPayments struct {
	bun.BaseModel      `bun:",alias:op"`
	ID                 uuid.UUID `bun:"id,pk,type:uuid"`
	OrderID            uuid.UUID
	PaymentType        string
	InstallmentsAmount uint
	Amount             uint
	CreatedAt          time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt          time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	DeletedAt          time.Time `bun:",soft_delete,nullzero"`
}

type OrderStatus struct {
	ID        uint `bun:",pk,autoincrement"`
	Status    string
	OrderID   uint
	CreatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	DeletedAt time.Time `bun:",soft_delete,nullzero"`
}

type CompanyOrdersInfoQty struct {
	OrdersMonthQty                     int64
	OrdersWeekQty                      int64
	OrdersTodayQty                     int64
	OrdersPaymentPendingQty            int64
	OrdersPaymentCreditCardQty         int64
	OrdersPaymentMoneyQty              int64
	OrdersPaymentDebitCardQty          int64
	OrdersTotalAmountLastMonth         int64
	OrdersTotalAmountLastWeek          int64
	OrdersTotalAmountToday             int64
	OrdersTotalAmountPaymentCreditCard int64
	OrdersTotalAmountPaymentMoney      int64
	OrdersTotalAmountPaymentDebitCard  int64
	OrdersTotalAmountPaymentPending    int64
	LastRegisteredOrders               []Order
}

var _ bun.BeforeAppendModelHook = (*Order)(nil)

func (c *Order) BeforeAppendModel(ctx context.Context, query bun.Query) error {
	switch query.(type) {
	case *bun.InsertQuery:
		if uuid, err := uuid.NewV4(); err != nil {
			return err
		} else {
			c.ID = uuid
		}
		c.CreatedAt = time.Now()
		c.UpdatedAt = time.Now()
	case *bun.UpdateQuery:
		c.UpdatedAt = time.Now()
	}
	return nil
}

var _ bun.BeforeAppendModelHook = (*OrdersPayments)(nil)

func (c *OrdersPayments) BeforeAppendModel(ctx context.Context, query bun.Query) error {
	switch query.(type) {
	case *bun.InsertQuery:
		if uuid, err := uuid.NewV4(); err != nil {
			return err
		} else {
			c.ID = uuid
		}
		c.CreatedAt = time.Now()
		c.UpdatedAt = time.Now()
	case *bun.UpdateQuery:
		c.UpdatedAt = time.Now()
	}
	return nil
}
