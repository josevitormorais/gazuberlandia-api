package models

import (
	"time"

	"github.com/uptrace/bun"
)

type Companie struct {
	bun.BaseModel `bun:",alias:cp"`
	ID            uint `bun:",pk,autoincrement"`
	UserID        uint
	Email         string
	Active        bool
	FantasyName   string
	Cnpj          string
	Phone         string
	Address       *CompaniesAddresses `bun:"rel:has-one,join:id=company_id"`
	CreatedAt     time.Time           `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt     time.Time           `bun:",nullzero,notnull,default:current_timestamp"`
	DeletedAt     time.Time           `bun:",soft_delete,nullzero"`
}

type CompaniesAddresses struct {
	bun.BaseModel `bun:",alias:cpa"`
	ID            uint `bun:",pk,autoincrement"`
	Street        string
	Neighborhood  string
	City          string
	State         string
	Complement    string
	Number        uint
	CompanyID     uint
	CreatedAt     time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt     time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	DeletedAt     time.Time `bun:",soft_delete,nullzero"`
}
