package models

import (
	"context"
	"time"

	"github.com/gofrs/uuid"
	"github.com/uptrace/bun"
)

type Product struct {
	bun.BaseModel    `bun:",alias:p"`
	ID               uuid.UUID `bun:",pk,type:uuid"`
	Active           bool
	Name             string
	Price            int64
	Description      string
	CompanyID        uint
	SalePrice        int64
	FileName         string
	OriginalFileName string
	Stock            *ProductsStock `bun:"rel:has-one,join:id=product_id"`
	CreatedAt        time.Time      `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt        time.Time      `bun:",nullzero,notnull,default:current_timestamp"`
	DeletedAt        time.Time      `bun:",soft_delete,nullzero"`
}

type ProductsStock struct {
	bun.BaseModel `bun:",alias:ps"`
	ID            uint      `bun:"id,pk,autoincrement"`
	ProductID     uuid.UUID `bun:",type:uuid"`
	Quantity      int
	CreatedAt     time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt     time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	DeletedAt     time.Time `bun:",soft_delete,nullzero"`
}

var _ bun.BeforeAppendModelHook = (*Product)(nil)
var _ bun.BeforeAppendModelHook = (*ProductsStock)(nil)

func (c *Product) BeforeAppendModel(ctx context.Context, query bun.Query) error {
	switch query.(type) {
	case *bun.InsertQuery:
		if uuid, err := uuid.NewV4(); err != nil {
			return err
		} else {
			c.ID = uuid
		}
		c.CreatedAt = time.Now()
		c.UpdatedAt = time.Now()
	case *bun.UpdateQuery:
		c.UpdatedAt = time.Now()
	}
	return nil
}

func (c *ProductsStock) BeforeAppendModel(ctx context.Context, query bun.Query) error {
	switch query.(type) {
	case *bun.InsertQuery:
		c.CreatedAt = time.Now()
		c.UpdatedAt = time.Now()
	case *bun.UpdateQuery:
		c.UpdatedAt = time.Now()
	}
	return nil
}
