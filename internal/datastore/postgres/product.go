package postgres

import (
	"context"
	"gazuberlandia/internal/datastore/postgres/models"
	"gazuberlandia/internal/domain"
	"github.com/rs/zerolog"

	"github.com/gofrs/uuid"
	"github.com/uptrace/bun"
)

func (p *postgres) CreateProduct(ctx context.Context, companyID uint, product *domain.Product) error {

	log := domain.Logger(p.logger, ctx)

	err := p.DB.RunInTx(ctx, nil, func(ctx context.Context, tx bun.Tx) error {
		pd := models.Product{
			Active:           !product.Active,
			CompanyID:        companyID,
			Name:             product.Name,
			Description:      product.Description,
			Price:            product.Price,
			SalePrice:        product.SalePrice,
			OriginalFileName: product.OriginalFileName,
			FileName:         product.GetFileName(),
		}

		_, err := tx.NewInsert().Model(&pd).Exec(ctx, &pd)

		if p.handleQueryError(err) != nil {
			log.Error().Err(err).Msg("error when trying insert a new product in database.")
			return err
		}
		return insertProductStock(ctx, p, pd.ID, product.Quantity, tx, log)
	})

	return err
}

func (p *postgres) DeleteProduct(ctx context.Context, id uuid.UUID) error {
	log := domain.Logger(p.logger, ctx)

	err := p.DB.RunInTx(ctx, nil, func(ctx context.Context, tx bun.Tx) error {
		_, err := tx.NewDelete().Model(&models.Product{ID: id}).WherePK().Exec(ctx)

		if err != nil {
			log.Error().Err(err).Msg("error when trying delete a product in database.")
			return err
		}

		_, err = tx.NewDelete().Model(&models.ProductsStock{ProductID: id}).Exec(ctx)

		if err != nil {
			log.Error().Err(err).Msg("error when delete stock of product in database.")
			return err
		}

		return nil
	})

	return err
}

func (p *postgres) UpdateProduct(ctx context.Context, companyID uint, product *domain.UpdateProduct) error {
	log := domain.Logger(p.logger, ctx)

	pd := &models.Product{
		ID:               product.ID,
		CompanyID:        companyID,
		Active:           product.Active,
		Name:             product.Name,
		Price:            product.Price,
		Description:      product.Description,
		SalePrice:        product.SalePrice,
		OriginalFileName: product.OriginalFileName,
	}

	if product.IsNewProductFile() {
		pd.FileName = product.GetFileName()
	}

	err := p.DB.RunInTx(ctx, nil, func(ctx context.Context, tx bun.Tx) error {

		_, err := tx.NewUpdate().
			Model(pd).
			Where("company_id = ?", &companyID).
			WherePK().
			Exec(ctx)

		if err != nil {
			log.Error().Err(err).Msg("error when trying update a product in database.")
			return err
		}

		_, err = tx.NewUpdate().
			Model(
				&models.ProductsStock{
					ProductID: product.ID,
					Quantity:  int(product.Stock.Quantity),
				}).
			WherePK("product_id").
			Exec(ctx)

		if err != nil {
			log.Error().Err(err).Msg("error when trying stock of product in database.")
			return err
		}

		return nil
	})

	return err
}

func (p *postgres) FindProductById(ctx context.Context, id uuid.UUID, companyID uint) (*models.Product, error) {
	pd := models.Product{
		ID: id,
	}
	err := p.DB.NewSelect().Model(&pd).Relation("Stock").Where("company_id = ?", &companyID).WherePK().Scan(ctx, &pd)
	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error when trying find product by id.")
		return nil, err
	}
	return &pd, nil
}

func (p *postgres) GetAllProduct(ctx context.Context, companyID uint) (*[]models.Product, error) {
	var pd []models.Product

	err := p.DB.NewSelect().Model(&pd).Relation("Stock").Where("company_id = ?", &companyID).Scan(ctx, &pd)
	if err != nil {
		return nil, err
	}
	return &pd, nil
}

func insertProductStock(ctx context.Context, p *postgres, id uuid.UUID, qty int, tx bun.Tx, log *zerolog.Logger) error {
	pds := models.ProductsStock{
		ProductID: id,
		Quantity:  qty,
	}
	_, err := tx.NewInsert().Model(&pds).Exec(ctx)
	if p.handleQueryError(err) != nil {
		log.Error().Err(err).Msg("error when trying insert a new stock to product.")
		return err
	}

	return nil
}

func (p *postgres) SearchProducts(ctx context.Context, companyID, userID, page, pageSize uint, query string) (*[]models.Product, error) {

	var pd []models.Product

	paginate := p.Paginate(page, pageSize)

	err := p.DB.NewSelect().
		Model(&pd).
		Join(`INNER JOIN companies cp ON cp.id = p.company_id`).
		Join(`INNER JOIN users u ON u.id = cp.user_id`).
		Where(`u.id = ?`, userID).
		Where(`cp.id = ?`, companyID).
		WhereGroup(" AND ", func(q *bun.SelectQuery) *bun.SelectQuery {
			return q.WhereOr(`p.id::text ILIKE concat('%',?,'%')`, query).
				WhereOr(`lower(regexp_replace(unaccent(p.description), '\W+', '', 'g')) ILIKE concat('%',lower(regexp_replace(unaccent(?), '\W+', '', 'g')),'%')`, query).
				WhereOr(`lower(regexp_replace(unaccent(p.name), '\W+', '', 'g')) ILIKE concat('%',lower(regexp_replace(unaccent(?), '\W+', '', 'g')),'%')`, query)

		}).
		Relation("Stock").
		Limit(paginate["limit"]).
		Offset(paginate["offset"]).
		Scan(ctx, &pd)

	if err != nil {
		domain.Logger(p.logger, ctx).Trace().Msg("error when searching products.")
		return nil, err
	}

	return &pd, nil
}

func (p *postgres) SearchProductsPaginated(ctx context.Context, userID, companyID, page, pageSize uint) (*[]models.Product, error) {

	var pd []models.Product

	paginate := p.Paginate(page, pageSize)

	err := p.DB.NewSelect().
		Model(&pd).
		Join(`INNER JOIN companies cp ON cp.id = p.company_id`).
		Join(`INNER JOIN users u ON u.id = cp.user_id`).
		Where(`u.id = ?`, userID).
		Where(`cp.id = ?`, companyID).
		Relation("Stock").
		Limit(paginate["limit"]).
		Offset(paginate["offset"]).
		Order("p.created_at DESC").
		Scan(ctx, &pd)

	if err != nil {
		domain.Logger(p.logger, ctx).Trace().Msg("error when pagination searching products.")
		return nil, err
	}

	return &pd, nil
}
