package postgres

import (
	"context"
	"gazuberlandia/internal/datastore/postgres/models"
	"gazuberlandia/internal/domain"

	"github.com/uptrace/bun"
)

func (p *postgres) CreateCustomer(ctx context.Context, customer *domain.CreateCustomerRequest) error {

	log := domain.Logger(p.logger, ctx)

	err := p.DB.RunInTx(ctx, nil, func(ctx context.Context, tx bun.Tx) error {

		c := &models.Customer{
			Name:       customer.Name,
			Email:      customer.Email,
			Active:     customer.Active,
			Phone:      customer.Phone,
			Cpf:        customer.Cpf,
			CompanyID:  customer.CompanyID,
			IsWhatsapp: customer.IsWhatsapp,
		}

		_, err := tx.NewInsert().
			Model(c).
			Returning("*").
			Exec(ctx)

		if err != nil {
			log.Error().Err(err).Msg("error trying when create a new customer in database.")
			return err
		}

		a := &models.CustomersAddresses{
			Street:       customer.Address.Street,
			Neighborhood: customer.Address.Neighborhood,
			City:         customer.Address.City,
			State:        customer.Address.State,
			Complement:   customer.Address.Complement,
			Number:       customer.Address.Number,
			CustomerID:   c.ID,
		}
		_, err = tx.NewInsert().
			Model(a).
			Exec(ctx)

		if err != nil {
			log.Error().Err(err).Msg("error trying when create a address of customer in database.")
			return err
		}

		return nil
	})

	return err
}

func (p *postgres) UpdateCustomer(ctx context.Context, customer *domain.Customer, companyID, id uint) error {
	log := domain.Logger(p.logger, ctx)

	cts := models.Customer{
		ID:         id,
		Name:       customer.Name,
		Email:      customer.Email,
		Active:     customer.Active,
		Phone:      customer.Phone,
		Cpf:        customer.Cpf,
		CompanyID:  companyID,
		IsWhatsapp: customer.IsWhatsapp,
	}

	err := p.DB.RunInTx(ctx, nil, func(ctx context.Context, tx bun.Tx) error {

		_, err := p.DB.NewUpdate().
			Model(&cts).
			ExcludeColumn("company_id", "id", "created_at").
			Where("ct.company_id = ?", companyID).
			WherePK().
			Exec(ctx)

		if err != nil {
			log.Error().Err(err).Msg("error trying when update customer in database.")
			return err
		}

		ctsAdd := models.CustomersAddresses{
			Street:       customer.Address.Street,
			Neighborhood: customer.Address.Neighborhood,
			City:         customer.Address.City,
			State:        customer.Address.State,
			Complement:   customer.Address.Complement,
			Number:       customer.Address.Number,
			CustomerID:   cts.ID,
		}
		_, err = tx.NewUpdate().
			Model(&ctsAdd).
			ExcludeColumn("customer_id", "id", "created_at").
			Where("customer_id = ?", cts.ID).
			Exec(ctx)

		if err != nil {
			log.Error().Err(err).Msg("error trying when update address of customer in database.")
			return err
		}

		return nil

	})

	return err

}

func (p *postgres) DeleteCustomer(ctx context.Context, id uint) error {
	log := domain.Logger(p.logger, ctx)

	_, err := p.DB.NewDelete().
		Model(&models.Customer{ID: id}).
		WherePK().
		Exec(ctx)

	if err != nil {
		log.Error().Err(err).Msg("error trying when delete customer in database.")
		return err
	}
	return nil
}

func (p *postgres) DeleteCustomerAddress(ctx context.Context, id uint) error {
	log := domain.Logger(p.logger, ctx)

	_, err := p.DB.NewDelete().
		Model(&models.CompaniesAddresses{ID: id}).
		WherePK().
		Exec(ctx)

	if err != nil {
		log.Error().Err(err).Msg("error trying when delete address from customer in database.")
		return err
	}
	return nil
}

func (p *postgres) GetAllCustomer(ctx context.Context, companyID uint) (*[]models.Customer, error) {
	var cs []models.Customer

	err := p.DB.NewSelect().
		Model(&cs).
		Relation("Address").
		Where("company_id = ?", &companyID).
		Order("updated_at DESC").
		Scan(ctx, &cs)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error trying get all customer from database.")
		return nil, err
	}

	return &cs, nil
}

func (p *postgres) SearchCustomerPaginated(ctx context.Context, page, pageSize, companyID uint, withCalc bool) (*[]models.Customer, error) {
	var cs []models.Customer

	paginate := p.Paginate(page, pageSize)

	err := p.DB.NewSelect().
		Model(&cs).
		Apply(func(sq *bun.SelectQuery) *bun.SelectQuery {
			if !withCalc {
				return sq
			}

			return sq.ColumnExpr("COALESCE(SUM(o.total_amount),0) AS amount_purchased").
				ColumnExpr("COUNT(o.id) AS purchased_qty").
				ColumnExpr("ct.*").
				Join("LEFT JOIN orders o ON o.customer_id = ct.id")
		}).
		Relation("Address").
		Where("ct.company_id = ?", &companyID).
		Group("ct.id", "address.id").
		Order("ct.created_at DESC").
		Limit(paginate["limit"]).
		Offset(paginate["offset"]).
		Scan(ctx, &cs)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error when trying to search customer paginated in database.")
		return nil, err
	}

	return &cs, nil
}

func (p *postgres) SearchCustomer(ctx context.Context, query domain.CustomerQueryParams, page, pageSize, companyID uint) (*[]models.Customer, error) {
	var cs []models.Customer

	paginate := p.Paginate(page, pageSize)

	err := p.DB.NewSelect().
		Model(&cs).
		Apply(func(sq *bun.SelectQuery) *bun.SelectQuery {
			if !query.Calc {
				return sq
			}

			return sq.ColumnExpr("COALESCE(SUM(o.total_amount),0) AS amount_purchased").
				ColumnExpr("COUNT(o.id) AS purchased_qty").
				Join("LEFT JOIN orders o ON o.customer_id = ct.id")
		}).
		Where("ct.company_id = ?", &companyID).
		Relation("Address").
		WhereGroup(" AND ", func(q *bun.SelectQuery) *bun.SelectQuery {
			return q.WhereOr(`ct.id::text ILIKE concat('%',?,'%')`, query.Query).
				WhereOr(`lower(regexp_replace(unaccent(ct.name), '\W+', '', 'g')) ILIKE concat('%',lower(regexp_replace(unaccent(?), '\W+', '', 'g')),'%')`, query.Query).
				WhereOr(`lower(regexp_replace(unaccent(ct.email), '\W+', '', 'g')) ILIKE concat('%',lower(regexp_replace(unaccent(?), '\W+', '', 'g')),'%')`, query.Query).
				WhereOr(`lower(regexp_replace(unaccent(ct.phone), '\W+', '', 'g')) ILIKE concat('%',lower(regexp_replace(unaccent(?), '\W+', '', 'g')),'%')`, query.Query).
				WhereOr(`lower(regexp_replace(unaccent(ct.cpf), '\W+', '', 'g')) ILIKE concat('%',lower(regexp_replace(unaccent(?), '\W+', '', 'g')),'%')`, query.Query).
				WhereOr(`lower(regexp_replace(unaccent(address.neighborhood), '\W+', '', 'g')) ILIKE concat('%',lower(regexp_replace(unaccent(?), '\W+', '', 'g')),'%')`, query.Query).
				WhereOr(`lower(regexp_replace(unaccent(address.street), '\W+', '', 'g')) ILIKE concat('%',lower(regexp_replace(unaccent(?), '\W+', '', 'g')),'%')`, query.Query)

		}).
		Limit(paginate["limit"]).
		Offset(paginate["offset"]).
		Order("ct.created_at DESC").
		Group("address.id", "ct.created_at", "ct.id").
		Scan(ctx, &cs)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error when trying to search customer in database.")
		return nil, err
	}

	return &cs, nil
}

func (p *postgres) FindCustomerById(ctx context.Context, id uint) (*models.Customer, error) {
	customer := models.Customer{
		ID: id,
	}

	err := p.DB.NewSelect().
		Model(&customer).
		Relation("Address").
		WherePK().
		Scan(ctx)

	if err != nil {
		domain.Logger(p.logger, ctx).Error().Err(err).Msg("error when trying to find customer by id.")
		return nil, err
	}

	return &customer, nil
}
