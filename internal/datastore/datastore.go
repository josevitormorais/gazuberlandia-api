package datastore

import (
	"context"
	"gazuberlandia/internal/datastore/postgres/models"
	"gazuberlandia/internal/domain"

	"github.com/gofrs/uuid"
)

type DataStore interface {
	userService
	orderService
	customerService
	companiesService
	productService
}

type userService interface {
	CreateUser(ctx context.Context, user *domain.CreateUserRequest) (*models.User, error)
	CreateRegister(ctx context.Context, register *domain.CreateRegister) error
	UpdateUser(ctx context.Context, userId int, user *domain.UpdateUserRequest) error
	DeleteUser(ctx context.Context, userId int) error
	FindUserById(ctx context.Context, id int) (*models.User, error)
	FindUserByEmail(ctx context.Context, email string, withCP bool) (*models.User, error)
	GetUserLogged(ctx context.Context, userID int, cpAddress bool) (*models.User, error)
}

type customerService interface {
	CreateCustomer(ctx context.Context, customer *domain.CreateCustomerRequest) error
	UpdateCustomer(ctx context.Context, customer *domain.Customer, companyID, id uint) error
	DeleteCustomer(ctx context.Context, id uint) error
	DeleteCustomerAddress(ctx context.Context, id uint) error
	FindCustomerById(ctx context.Context, id uint) (*models.Customer, error)
	GetAllCustomer(ctx context.Context, companyID uint) (*[]models.Customer, error)
	SearchCustomer(ctx context.Context, query domain.CustomerQueryParams, page, pageSize, companyID uint) (*[]models.Customer, error)
	SearchCustomerPaginated(ctx context.Context, page, pageSize, companyID uint, withCalc bool) (*[]models.Customer, error)
}

type companiesService interface {
	CreateCompany(ctx context.Context, userID uint, companie *domain.CreateCompaniesRequest) error
	FindCompanyById(ctx context.Context, id uint, userID uint) (*models.Companie, error)
	GetAllCompanyFromUserID(ctx context.Context, userID uint) (*[]models.Companie, error)
}

type orderService interface {
	CreateOrder(ctx context.Context, order *domain.Order, companyID uint) error
	UpdateOrder(ctx context.Context, order *domain.OrderUpdated, orderID uuid.UUID, userID, companyID uint) error
	FindOrderByID(ctx context.Context, orderID uuid.UUID, userID, companyID uint) (*models.Order, error)
	GetAllOrder(ctx context.Context, userID, companyID uint) ([]models.Order, error)
	SearchOrderPaginated(ctx context.Context, page, pageSize, userID, companyID uint) ([]models.Order, error)
	SearchOrder(ctx context.Context, q *domain.OrderQueryParams, page, pageSize, userID, companyID uint) ([]models.Order, error)
	GetOrderCompanyInfo(ctx context.Context, userID, companyID uint) (*models.CompanyOrdersInfoQty, error)
}

type productService interface {
	CreateProduct(ctx context.Context, companyID uint, product *domain.Product) error
	DeleteProduct(ctx context.Context, id uuid.UUID) error
	UpdateProduct(ctx context.Context, companyID uint, product *domain.UpdateProduct) error
	FindProductById(ctx context.Context, id uuid.UUID, companyID uint) (*models.Product, error)
	GetAllProduct(ctx context.Context, companyID uint) (*[]models.Product, error)
	SearchProducts(ctx context.Context, companyID, userID, page, pageSize uint, query string) (*[]models.Product, error)
	SearchProductsPaginated(ctx context.Context, userID, companyID, page, pageSize uint) (*[]models.Product, error)
}
