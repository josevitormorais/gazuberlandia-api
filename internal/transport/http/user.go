package http

import (
	"encoding/json"
	"gazuberlandia/internal/domain"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

const (
	jsonErrMsg        string = "json format is invalid."
	emailOrNameErrMsg string = "Email or name already exists."
)

func (s *Server) handleCreateUser(e echo.Context) error {
	var user *domain.CreateUserRequest

	if err := e.Bind(&user); err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	userRes, err := s.Service.UserService.CreateUser(s.CtxWithRequestID(e), user)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	tk, err := domain.GenerateJWT(userRes.ID, s.config.JwtSecret)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, map[string]string{
		"token": tk,
	})
}

func (s *Server) handleUpdateUserById(e echo.Context) error {
	userId, err := strconv.Atoi(e.Param("id"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "internal server error")
	}

	var user *domain.UpdateUserRequest
	if err := e.Bind(&user); err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	err = s.Service.UserService.UpdateUser(s.CtxWithRequestID(e), userId, user)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.NoContent(http.StatusNoContent)
}

func (s *Server) handleDeleteUserById(e echo.Context) error {
	userId, err := strconv.Atoi(e.Param("id"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "internal server error")
	}

	err = s.Service.UserService.DeleteUser(s.CtxWithRequestID(e), userId)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	return e.NoContent(http.StatusNoContent)
}

func (s *Server) handleFindUserById(e echo.Context) error {
	userId, err := strconv.Atoi(e.Param("id"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "internal server error")
	}

	user, err := s.Service.UserService.FindUserById(s.CtxWithRequestID(e), userId)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, &user)
}

func (s *Server) handleRegister(e echo.Context) error {
	var r *domain.CreateRegister

	if err := json.NewDecoder(e.Request().Body).Decode(&r); err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	err := s.Service.UserService.CreateRegister(s.CtxWithRequestID(e), r)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.NoContent(http.StatusNoContent)
}
