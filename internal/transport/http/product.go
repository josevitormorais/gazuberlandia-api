package http

import (
	"encoding/json"
	"errors"
	"fmt"
	"gazuberlandia/internal/domain"
	"mime/multipart"
	"net/http"
	"strings"

	"github.com/gofrs/uuid"
	"github.com/labstack/echo/v4"
)

func isDigit(c rune) bool { return c < '0' || c > '9' }

func bindUploadProductData(form *multipart.Form) (*domain.Product, error) {
	var pd domain.Product

	formData := make(map[string]interface{})

	for v, i := range form.Value {
		var num int64
		b := strings.IndexFunc(i[0], isDigit) == -1
		if !b {
			formData[v] = i[0]
			continue
		}

		_, err := fmt.Sscan(i[0], &num)
		if err != nil {
			return nil, err
		}
		formData[v] = num
	}

	BJson, err := json.Marshal(formData)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(BJson, &pd); err != nil {
		return nil, err
	}

	return &pd, err
}

func (s *Server) handleCreateProduct(e echo.Context) error {

	// max size of form
	err := e.Request().ParseMultipartForm(32 << 20)
	if err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	err = e.Request().ParseForm()
	if err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	form, err := e.MultipartForm()
	if err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	pd, err := bindUploadProductData(form)
	if err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	f, err := e.FormFile("file")
	if err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)

	}

	cpID := e.Get("companyLogged").(uint)

	err = s.Service.ProductService.CreateProduct(s.CtxWithRequestID(e), cpID, pd, f)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.NoContent(http.StatusNoContent)
}

func (s *Server) handleUpdateProduct(e echo.Context) error {
	pd := new(domain.UpdateProduct)

	err := e.Request().ParseMultipartForm(32 << 20)
	if err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	form, err := e.MultipartForm()
	if err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)

	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &pd)

	if err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	ff, err := e.FormFile("file")
	if err != nil && !errors.Is(err, http.ErrMissingFile) {
		return HandleJsonHTTPError(http.StatusBadRequest, err)

	}

	cpID := e.Get("companyLogged").(uint)

	err = s.Service.ProductService.UpdateProduct(s.CtxWithRequestID(e), cpID, pd, ff)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.NoContent(http.StatusNoContent)
}

func (s *Server) handleFindProductById(e echo.Context) error {
	productId := uuid.FromStringOrNil(e.Param("id"))

	cpID := e.Get("companyLogged").(uint)

	c, err := s.Service.ProductService.FindProductById(s.CtxWithRequestID(e), productId, cpID)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return e.JSON(http.StatusOK, c)
}

func (s *Server) handleGetAllProduct(e echo.Context) error {

	cpID := e.Get("companyLogged").(uint)

	pd, err := s.Service.ProductService.GetAllProduct(s.CtxWithRequestID(e), cpID)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, pd)
}

func (s *Server) handleDeleteProductById(e echo.Context) error {
	productId := uuid.FromStringOrNil(e.Param("id"))

	err := s.Service.ProductService.DeleteProduct(s.CtxWithRequestID(e), productId)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return e.NoContent(http.StatusNoContent)
}

func (s *Server) searchProducts(e echo.Context) error {
	userLogged := e.Get("userLogged").(*domain.UserResponse)
	uID, cpID := userLogged.ID, e.Get("companyLogged").(uint)

	p, err := s.getPaginateParams(e)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	pd, err := s.Service.ProductService.SearchProducts(
		s.CtxWithRequestID(e),
		cpID,
		uID,
		p["page"],
		p["pageSize"],
		e.QueryParam("query"),
	)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, pd)

}

func (s *Server) searchProductPaginated(e echo.Context) error {

	userLogged := e.Get("userLogged").(*domain.UserResponse)
	uID, cpID := userLogged.ID, e.Get("companyLogged").(uint)

	p, err := s.getPaginateParams(e)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	pd, err := s.Service.ProductService.SearchProductsPaginated(
		s.CtxWithRequestID(e),
		uID,
		cpID,
		p["page"],
		p["pageSize"],
	)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, pd)
}
