package http

import (
	"net/http"
	"strconv"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

func (s *Server) GetLoggedUser(next echo.HandlerFunc) echo.HandlerFunc {
	return func(e echo.Context) error {
		user := e.Get("user").(*jwt.Token)
		claims := user.Claims.(jwt.MapClaims)
		userID := claims["user_id"].(float64)

		isShouldCheckCpID := s.shouldValidateHeaderCompany(e.Request())

		userLogged, err := s.Service.UserService.GetUserLogged(s.CtxWithRequestID(e), int(userID), false)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err)
		}

		if !isShouldCheckCpID {
			e.Set("userLogged", userLogged)
			return next(e)
		}

		cpID, err := strconv.ParseUint(e.Request().Header["X-Tenant-Id"][0], 10, 64)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err)
		}

		var cpBelongsToUser bool
		for _, cp := range *userLogged.Companies {
			if cp.ID == uint(cpID) {
				cpBelongsToUser = true
				break
			}
		}

		if !cpBelongsToUser {
			return s.handleErrorForbidden()
		}

		e.Set("companyLogged", uint(cpID))
		e.Set("userLogged", userLogged)
		return next(e)
	}
}

func (s *Server) validateHeaderCompany(next echo.HandlerFunc) echo.HandlerFunc {
	return func(e echo.Context) error {
		isCheck := s.shouldValidateHeaderCompany(e.Request())

		headerCpID := e.Request().Header["X-Tenant-Id"]

		if isCheck && len(headerCpID) == 0 {
			return s.handleErrorForbidden()
		}

		return next(e)
	}
}
