package http

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func (s *Server) registerCustomerRoutes(e *echo.Echo) {
	r := e.Group("/customers")

	r.Use(s.GetLoggedUser)
	r.POST("", s.handleCreateCustomer)

	r.GET("", s.handleGetAllCustomer)
	r.GET("/:id", s.handleFindCustomerById)
	r.GET("/search", s.handleSearchCustomer)
	r.GET("/search/paginated", s.handleSearchCustomerPaginated)

	r.PUT("/:id", s.handleUpdateCustomer)

	r.DELETE("/:id", s.handleDeleteCustomerById)
	r.DELETE("/address/:id", s.handleDeleteCustomerAddressById)
}

func (s *Server) registerOrderRouter(e *echo.Echo) {
	r := e.Group("/orders")

	r.Use(s.GetLoggedUser)

	r.POST("", s.handleCreateOrder)

	r.PATCH("/:id", s.handleUpdateOrder)

	r.GET("", s.handleGetAllOrder)
	r.GET("/search/paginated", s.searchOrderPaginated)
	r.GET("/search", s.searchOrder)
	r.GET("/:id", s.handleFindOrderByID)
	r.GET("/info", s.handleGetCompanyInfo)

}

func (s *Server) registerProductRouter(e *echo.Echo) {
	r := e.Group("/products")

	r.Use(s.GetLoggedUser)
	r.POST("", s.handleCreateProduct)

	r.PUT("", s.handleUpdateProduct)
	r.PUT("/:id", s.handleUpdateProduct)

	r.GET("", s.handleGetAllProduct)
	r.GET("/search", s.searchProducts)
	r.GET("/search/paginated", s.searchProductPaginated)
	r.GET("/:id", s.handleFindProductById)

	r.DELETE("/:id", s.handleDeleteProductById)

}

func (s *Server) registerUserRoutes(e *echo.Echo) {
	r := e.Group("/users")

	r.POST("/register/", s.handleRegister)

	r.Use(s.GetLoggedUser)

	r.POST("/", s.handleCreateUser)
	r.GET("/:id", s.handleFindUserById)
	r.PUT("/:id", s.handleUpdateUserById)
	r.DELETE("/:id", s.handleDeleteUserById)

}

func (s *Server) registerCompaniesRoutes(e *echo.Echo) {
	r := e.Group("/companies")

	r.Use(s.GetLoggedUser)
	r.GET("", s.handleGetCompanyFromUserLogged)
	r.GET("/", s.handleGetAllCompanyFromUserID)
	r.GET("/:id", s.handleFindCompanyById)
	r.POST("", s.handleCreateCompanie)

	// TODO PUT companies
	// r.PATCH("/:id", s.handleFindCompanyById)
}

func (s *Server) registerAuthRoutes(e *echo.Echo) {
	e.POST("/login", s.handleLogin)
}

func (s *Server) registerVersion(e *echo.Echo) {
	r := e.Group("/version")

	r.GET("", func(e echo.Context) error {
		return e.JSON(http.StatusOK, map[string]string{"vesion": s.Version})
	})
}
