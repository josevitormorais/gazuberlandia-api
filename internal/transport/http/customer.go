package http

import (
	"gazuberlandia/internal/domain"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func (s *Server) handleCreateCustomer(e echo.Context) error {
	var customer *domain.CreateCustomerRequest

	if err := e.Bind(&customer); err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	customer.CompanyID = e.Get("companyLogged").(uint)

	err := s.Service.CustomerService.CreateCustomer(s.CtxWithRequestID(e), customer)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.NoContent(http.StatusNoContent)

}

func (s *Server) handleUpdateCustomer(e echo.Context) error {
	cID, err := strconv.ParseUint(e.Param("id"), 10, 64)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "internal server error")
	}

	var customer *domain.Customer
	if err := e.Bind(&customer); err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	cpID := e.Get("companyLogged").(uint)

	err = s.Service.CustomerService.
		UpdateCustomer(s.CtxWithRequestID(e), customer, cpID, uint(cID))

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.NoContent(http.StatusNoContent)

}

func (s *Server) handleDeleteCustomerById(e echo.Context) error {
	customerId, err := strconv.ParseUint(e.Param("id"), 10, 64)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "internal server error")
	}
	err = s.Service.CustomerService.DeleteCustomer(s.CtxWithRequestID(e), uint(customerId))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return e.NoContent(http.StatusNoContent)
}

func (s *Server) handleDeleteCustomerAddressById(e echo.Context) error {
	customerId, err := strconv.ParseUint(e.Param("id"), 10, 64)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "internal server error")
	}
	err = s.Service.CustomerService.DeleteCustomerAddress(s.CtxWithRequestID(e), uint(customerId))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return e.NoContent(http.StatusNoContent)
}

func (s *Server) handleFindCustomerById(e echo.Context) error {
	cID, err := strconv.ParseUint(e.Param("id"), 10, 64)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "internal server error")
	}
	c, err := s.Service.CustomerService.FindCustomerById(s.CtxWithRequestID(e), uint(cID))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return e.JSON(http.StatusOK, c)
}

func (s *Server) handleGetAllCustomer(e echo.Context) error {

	cpID := e.Get("companyLogged").(uint)

	c, err := s.Service.CustomerService.GetAllCustomer(s.CtxWithRequestID(e), cpID)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, c)
}

func (s *Server) handleSearchCustomer(e echo.Context) error {

	cpID := e.Get("companyLogged").(uint)

	p, err := s.getPaginateParams(e)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	oQuery := domain.CustomerQueryParams{}

	err = echo.QueryParamsBinder(e).
		Bool("calc", &oQuery.Calc).
		String("query", &oQuery.Query).
		BindError()

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	c, err := s.Service.CustomerService.SearchCustomer(
		s.CtxWithRequestID(e),
		oQuery,
		p["page"],
		p["pageSize"],
		cpID,
	)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, c)
}
func (s *Server) handleSearchCustomerPaginated(e echo.Context) error {

	cpID := e.Get("companyLogged").(uint)

	oQuery := domain.CustomerQueryParams{}

	err := echo.QueryParamsBinder(e).
		Bool("calc", &oQuery.Calc).
		BindError()

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	p, err := s.getPaginateParams(e)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	c, err := s.Service.CustomerService.SearchCustomerPaginated(
		s.CtxWithRequestID(e),
		p["page"],
		p["pageSize"],
		cpID,
		oQuery.Calc,
	)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, c)
}
