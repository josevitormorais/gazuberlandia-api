package http

import (
	"gazuberlandia/internal/domain"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func (s *Server) handleCreateCompanie(e echo.Context) error {
	var c *domain.CreateCompaniesRequest

	if err := e.Bind(&c); err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	userLogged := e.Get("userLogged").(*domain.UserResponse)

	err := s.Service.CompaniesService.CreateCompany(s.CtxWithRequestID(e), userLogged.ID, c)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.NoContent(http.StatusNoContent)
}

func (s *Server) handleGetAllCompanyFromUserID(e echo.Context) error {

	userLogged := e.Get("userLogged").(*domain.UserResponse)

	cps, err := s.Service.CompaniesService.GetAllCompanyFromUserID(s.CtxWithRequestID(e), userLogged.ID)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, &cps)
}

func (s *Server) handleFindCompanyById(e echo.Context) error {
	cpID, err := strconv.Atoi(e.Param("id"))

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "internal server error")
	}

	uID := e.Get("userLogged").(*domain.UserResponse).ID

	c, err := s.Service.CompaniesService.FindCompanyById(s.CtxWithRequestID(e), uint(cpID), uID)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, &c)
}

func (s *Server) handleGetCompanyFromUserLogged(e echo.Context) error {
	uID := e.Get("userLogged").(*domain.UserResponse).ID

	c, err := s.Service.UserService.GetUserLogged(s.CtxWithRequestID(e), int(uID), true)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, &c)
}
