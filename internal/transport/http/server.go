package http

import (
	"context"
	"gazuberlandia/internal/config"
	"gazuberlandia/internal/datastore"
	"gazuberlandia/internal/domain"
	"gazuberlandia/internal/services"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/rs/zerolog"
)

type Server struct {
	Version     string
	SessionName string
	*http.Server

	Service  *services.Service
	Echo     *echo.Echo
	validate *validator.Validate
	config   *config.Config
}

type PaginatedParams struct {
	Page     uint
	PageSize uint
}

func NewServer(c *config.Config, options ...func(*Server)) *Server {
	server := &Server{}
	server.Version = "v1"
	server.config = c

	for _, optionsFn := range options {
		optionsFn(server)
	}

	return server
}

func NewRouter(logger *zerolog.Logger) func(*Server) {
	return func(srv *Server) {
		e := echo.New()

		e.Use(middleware.BodyLimit("4M"))

		e.Use(middleware.RateLimiter(middleware.NewRateLimiterMemoryStore(20)))

		e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
			AllowOrigins: srv.config.Cors,
			AllowMethods: middleware.DefaultCORSConfig.AllowMethods,
		}))

		timeoutMiddleware := middleware.TimeoutWithConfig(middleware.TimeoutConfig{
			Timeout: time.Duration(srv.config.Timeout) * time.Millisecond,
		})

		e.Use(middleware.Recover(), middleware.RequestID(), timeoutMiddleware)

		e.Use(middleware.JWTWithConfig(middleware.JWTConfig{
			SigningKey: []byte(srv.config.JwtSecret),
			Skipper: func(e echo.Context) bool {
				skip := false
				pathsWithoutAuth := []string{"/users/register/", "/login"}

				for _, path := range pathsWithoutAuth {
					if isEqual := strings.Compare(path, e.Request().RequestURI); isEqual == 0 {
						skip = true
					}
				}

				return skip
			},
		}))

		e.Use(srv.validateHeaderCompany)

		e.Use(middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
			LogURI:           true,
			LogStatus:        true,
			LogLatency:       true,
			LogProtocol:      true,
			LogRemoteIP:      true,
			LogHost:          true,
			LogMethod:        true,
			LogURIPath:       true,
			LogRoutePath:     true,
			LogRequestID:     true,
			LogReferer:       true,
			LogUserAgent:     true,
			LogError:         true,
			LogContentLength: true,
			LogResponseSize:  true,
			LogHeaders:       []string{"X-Tenant-Id"},
			LogValuesFunc: func(e echo.Context, v middleware.RequestLoggerValues) error {

				var userLogged = e.Get("userLogged")
				var userID string

				if userLogged != nil {
					userID = strconv.FormatUint(uint64(userLogged.(*domain.UserResponse).ID), 10)
				}

				logger.Info().
					Time("START_DATE", v.StartTime).
					Str("URI", v.URI).
					Str("HOST", v.Host).
					Str("X-Tenant-Id", v.Headers["X-Tenant-Id"][0]).
					Str("USER_ID", userID).
					Str("REMOTE_IP", v.RemoteIP).
					Str("USER_AGENT", v.UserAgent).
					Str("METHOD", v.Method).
					Int("STATUS", v.Status).
					Str("REQUEST_ID", v.RequestID).
					Msg("[REQUEST]")
				return nil
			},
		}))

		srv.registerVersion(e)

		srv.registerAuthRoutes(e)
		srv.registerUserRoutes(e)
		srv.registerOrderRouter(e)
		srv.registerCustomerRoutes(e)
		srv.registerCompaniesRoutes(e)
		srv.registerProductRouter(e)

		srv.Echo = e
	}
}

func AddServices(store datastore.DataStore, logger *zerolog.Logger) func(*Server) {
	return func(srv *Server) {
		srv.Service = services.NewServices(store, logger)
	}
}

func NewValidator() func(*Server) {
	return func(srv *Server) {
		srv.validate = validator.New()
	}
}

func NewConfigHttpServer() func(*Server) {
	return func(srv *Server) {
		srv.Server = &http.Server{
			Addr:         ":5000",
			Handler:      srv.Echo,
			ReadTimeout:  time.Second * 5,
			WriteTimeout: time.Second * 5,
			IdleTimeout:  time.Second * 10,
		}
	}
}

func (s *Server) CtxWithRequestID(c echo.Context) context.Context {
	ctx := context.WithValue(c.Request().Context(), "requestID", c.Response().Header().Get(echo.HeaderXRequestID))
	return ctx
}

func (s *Server) getPaginateParams(e echo.Context) (map[string]uint, error) {

	params := make(map[string]uint)

	pp := PaginatedParams{}

	err := echo.QueryParamsBinder(e).
		Uint("page", &pp.Page).
		Uint("pageSize", &pp.PageSize).
		BindError()

	if err != nil {
		return nil, err
	}

	params["page"] = pp.Page
	params["pageSize"] = pp.PageSize

	return params, nil

}

func (s *Server) handleErrorForbidden() *echo.HTTPError {
	return echo.NewHTTPError(http.StatusForbidden,
		map[string]string{"error": `you don’t have permission to access this resource`},
	)
}

func (s *Server) shouldValidateHeaderCompany(r *http.Request) bool {
	var isCheckPath = true

	for _, path := range []string{"/companies"} {
		if r.RequestURI == path && r.Method == "GET" {
			isCheckPath = false
			break
		}
	}
	return isCheckPath
}
