package http

import (
	"fmt"

	"github.com/labstack/echo/v4"
)

func HandleJsonHTTPError(code int, err error) *echo.HTTPError {
	return echo.NewHTTPError(code, fmt.Sprintf("invalid json format: %s", err.(*echo.HTTPError).Message))
}
