package http

import (
	"gazuberlandia/internal/domain"
	"net/http"

	"github.com/labstack/echo/v4"
)

func (s *Server) handleLogin(c echo.Context) error {
	var login *domain.LoginRequest

	if err := c.Bind(&login); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "Invalid json format")
	}

	err := s.validate.Struct(login)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	token, err := s.Service.AuthService.Login(s.CtxWithRequestID(c), login.Email, login.Password, s.config.JwtSecret)

	if err != nil || token == "" {
		return echo.NewHTTPError(http.StatusUnauthorized, err.Error())
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": token,
	})
}
