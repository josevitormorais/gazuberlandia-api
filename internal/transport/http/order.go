package http

import (
	"gazuberlandia/internal/domain"
	"net/http"

	"github.com/gofrs/uuid"
	"github.com/labstack/echo/v4"
)

func (s *Server) handleCreateOrder(e echo.Context) error {
	var o *domain.Order

	if err := e.Bind(&o); err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	err := s.validate.Struct(o)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	err = s.Service.OrderService.CreateOrder(s.CtxWithRequestID(e), o, e.Get("companyLogged").(uint))

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.NoContent(http.StatusNoContent)
}

func (s *Server) handleUpdateOrder(e echo.Context) error {
	var o *domain.OrderUpdated

	if err := e.Bind(&o); err != nil {
		return HandleJsonHTTPError(http.StatusBadRequest, err)
	}

	err := s.validate.Struct(o)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	err = s.Service.OrderService.UpdateOrder(s.CtxWithRequestID(e),
		o,
		uuid.FromStringOrNil(e.Param("id")),
		e.Get("userLogged").(*domain.UserResponse).ID,
		e.Get("companyLogged").(uint),
	)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.NoContent(http.StatusNoContent)
}

func (s *Server) handleFindOrderByID(e echo.Context) error {
	orderID := uuid.FromStringOrNil(e.Param("id"))

	userLogged := e.Get("userLogged").(*domain.UserResponse)
	uID, cpID := userLogged.ID, e.Get("companyLogged").(uint)

	pd, err := s.Service.OrderService.FindOrderByID(s.CtxWithRequestID(e), orderID, uID, cpID)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, pd)
}

func (s *Server) handleGetAllOrder(e echo.Context) error {

	userLogged := e.Get("userLogged").(*domain.UserResponse)
	uID, cpID := userLogged.ID, e.Get("companyLogged").(uint)

	or, err := s.Service.OrderService.GetAllOrder(s.CtxWithRequestID(e), uID, cpID)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, or)
}

func (s *Server) searchOrderPaginated(e echo.Context) error {

	userLogged := e.Get("userLogged").(*domain.UserResponse)
	uID, cpID := userLogged.ID, e.Get("companyLogged").(uint)

	p, err := s.getPaginateParams(e)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	or, err := s.Service.OrderService.SearchOrderPaginated(
		s.CtxWithRequestID(e),
		p["page"],
		p["pageSize"],
		uID,
		cpID,
	)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, or)
}

func (s *Server) searchOrder(e echo.Context) error {

	userLogged := e.Get("userLogged").(*domain.UserResponse)
	uID := userLogged.ID
	cpID := e.Get("companyLogged").(uint)

	p, err := s.getPaginateParams(e)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	oQuery := domain.OrderQueryParams{}

	err = echo.QueryParamsBinder(e).
		String("id", &oQuery.ID).
		String("status", &oQuery.Status).
		String("query", &oQuery.Query).
		String("payment_type", &oQuery.PaymentType).
		BindError()

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	or, err := s.Service.OrderService.SearchOrder(
		s.CtxWithRequestID(e),
		&oQuery,
		p["page"],
		p["pageSize"],
		uID,
		cpID,
	)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, or)
}

func (s *Server) handleGetCompanyInfo(e echo.Context) error {

	userLogged := e.Get("userLogged").(*domain.UserResponse)
	uID, cpID := userLogged.ID, e.Get("companyLogged").(uint)

	c, err := s.Service.OrderService.GetOrderCompanyInfo(s.CtxWithRequestID(e), uID, cpID)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return e.JSON(http.StatusOK, &c)
}
