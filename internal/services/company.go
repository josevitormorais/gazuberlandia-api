package services

import (
	"context"
	"gazuberlandia/internal/datastore"
	"gazuberlandia/internal/domain"

	"github.com/rs/zerolog"
)

type companyService struct {
	store datastore.DataStore
	log   *zerolog.Logger
}

func NewCompanyService(store datastore.DataStore, logger *zerolog.Logger) *companyService {
	newLog := logger.With().Str("Service", "[COMPANY_SERVICE]").Logger()
	return &companyService{store: store, log: &newLog}
}

func (c *companyService) CreateCompany(ctx context.Context, userID uint, companie *domain.CreateCompaniesRequest) error {
	log := domain.Logger(c.log, ctx)
	log.Trace().Msg("create a new company.")

	err := c.store.CreateCompany(ctx, userID, companie)

	if err != nil {
		log.Err(err).Msg("error to create a new company.")
		return FormatErr(err)
	}
	return nil
}

func (c *companyService) FindCompanyById(ctx context.Context, id uint, userID uint) (*domain.CompaniesResponse, error) {
	log := domain.Logger(c.log, ctx)
	log.Trace().Msg("find company by id.")

	cp, err := c.store.FindCompanyById(ctx, id, userID)

	if err != nil {
		log.Err(err).Msg("error trying to find company by id.")
		return nil, FormatErr(err)
	}

	return domain.CompanyResponseFromDataStore(*cp), nil
}

func (c *companyService) GetAllCompanyFromUserID(ctx context.Context, userID uint) (*[]domain.CompaniesResponse, error) {
	log := domain.Logger(c.log, ctx)
	log.Trace().Msg("get all companie from user.")

	companies, err := c.store.GetAllCompanyFromUserID(ctx, userID)

	if err != nil {
		return nil, FormatErr(err)
	}

	cps := domain.AllCompanies(*companies)

	if cps == nil {
		return &[]domain.CompaniesResponse{}, nil
	}

	return cps, nil
}
