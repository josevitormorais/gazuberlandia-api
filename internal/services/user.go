package services

import (
	"context"
	"errors"
	"gazuberlandia/internal/datastore"
	"gazuberlandia/internal/domain"

	"github.com/alexedwards/argon2id"
	"github.com/rs/zerolog"
)

type userService struct {
	store datastore.DataStore
	log   *zerolog.Logger
}

func NewUserService(store datastore.DataStore, logger *zerolog.Logger) *userService {
	newLog := logger.With().Str("Service", "[USER_SERVICE]").Logger()
	return &userService{store: store, log: &newLog}
}

func (u *userService) GetUserLogged(ctx context.Context, userID int, cpAddress bool) (*domain.UserResponse, error) {
	domain.Logger(u.log, ctx).Trace().Msg("Get logged user")

	user, err := u.store.GetUserLogged(ctx, userID, cpAddress)
	if err != nil {
		return nil, err
	}

	return domain.UserResponseFromDataStore(user), nil
}

func (u *userService) CreateUser(ctx context.Context, user *domain.CreateUserRequest) (*domain.UserResponse, error) {
	domain.Logger(u.log, ctx).Trace().Msg("create a new user")
	hash, err := argon2id.CreateHash(user.Password, argon2id.DefaultParams)

	if err != nil && hash == "" {
		domain.Logger(u.log, ctx).Error().Err(err).Msg("error when create hash for password")
		return nil, errors.New("internal server error")
	}
	user.Password = hash
	userModel, err := u.store.CreateUser(ctx, user)

	if err != nil {
		domain.Logger(u.log, ctx).Error().Err(err).Msg("error when create user")
		return nil, FormatErr(err)
	}

	return domain.UserResponseFromDataStore(userModel), nil
}

func (u *userService) CreateRegister(ctx context.Context, register *domain.CreateRegister) error {
	domain.Logger(u.log, ctx).Trace().Msg("create a new register")

	hash, err := argon2id.CreateHash(register.User.Password, argon2id.DefaultParams)

	if err != nil && hash == "" {
		domain.Logger(u.log, ctx).Error().Err(err).Msg("error when create hash for register")
		return errors.New("internal server error")
	}
	register.Password = hash
	err = u.store.CreateRegister(ctx, register)

	if err != nil {
		domain.Logger(u.log, ctx).Error().Err(err).Msg("error when register new user and company")
		return FormatErr(err)
	}

	return nil
}

func (u *userService) UpdateUser(ctx context.Context, userId int, user *domain.UpdateUserRequest) error {
	domain.Logger(u.log, ctx).Trace().Msg("update user.")

	err := u.store.UpdateUser(ctx, userId, user)
	if err != nil {
		domain.Logger(u.log, ctx).Error().Err(err).Msg("error trying to update user")
		return FormatErr(err)
	}
	return nil
}

func (u *userService) DeleteUser(ctx context.Context, userId int) error {
	domain.Logger(u.log, ctx).Trace().Msg("delete user.")

	err := u.store.DeleteUser(ctx, userId)
	if err != nil {
		domain.Logger(u.log, ctx).Error().Err(err).Msg("error trying to delete user")
		return FormatErr(err)
	}
	return nil
}

func (u *userService) FindUserById(ctx context.Context, id int) (*domain.UserResponse, error) {
	domain.Logger(u.log, ctx).Trace().Msg("find user by id.")

	user, err := u.store.FindUserById(ctx, id)

	if err != nil {
		domain.Logger(u.log, ctx).Error().Err(err).Msg("error find to user")
		return nil, FormatErr(err)
	}

	return domain.UserResponseFromDataStore(user), nil
}

func (u *userService) FindUserByEmail(ctx context.Context, email string, withCP bool) (*domain.UserResponse, error) {
	domain.Logger(u.log, ctx).Trace().Msg("find user by email.")

	user, err := u.store.FindUserByEmail(ctx, email, withCP)
	if err != nil {
		domain.Logger(u.log, ctx).Error().Err(err).Msg("error trying to find user by email")
		return nil, FormatErr(err)
	}
	return domain.UserResponseFromDataStore(user), nil
}
