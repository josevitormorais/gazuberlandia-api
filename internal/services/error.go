package services

import (
	"errors"
	"fmt"
	"log"
	"regexp"
	"strings"

	"github.com/jackc/pgconn"
)

func FormatErr(err error) error {

	if strings.Contains(err.Error(), "no rows in result set") {
		log.Printf("Error trying query on databse. Reason: %v", err)
		return errors.New("not found")
	}

	var e *pgconn.PgError
	var errMsg string

	errCode := err.(*pgconn.PgError).Code
	errDetail := err.(*pgconn.PgError).Detail
	tableName := err.(*pgconn.PgError).TableName

	if re, err := regexp.Compile(`(\bKey\b)|((=?=\()[^\s]+)|[()]`); err != nil {
		log.Fatal("Error to compile regex")
	} else {
		errMsg = fmt.Sprintf("%s %s", tableName, strings.TrimSpace(re.ReplaceAllString(errDetail, "")))
	}

	log.Printf("Error trying query on databse. Reason: %v", err)

	switch errors.As(err, &e) {
	case strings.Contains(errDetail, "already exists") && errCode == "23505":
		return errors.New(errMsg)
	default:
		return errors.New("internal server error")
	}
}
