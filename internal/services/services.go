package services

import (
	"gazuberlandia/internal/datastore"
	"gazuberlandia/internal/domain"

	"github.com/rs/zerolog"
)

type Service struct {
	AuthService      domain.Auth
	CustomerService  domain.CustomerService
	OrderService     domain.OrderService
	UserService      domain.UserService
	CompaniesService domain.CompaniesService
	ProductService   domain.ProductService
}

func NewServices(store datastore.DataStore, logger *zerolog.Logger) *Service {
	return &Service{
		UserService:      NewUserService(store, logger),
		OrderService:     NewOrderService(store, logger),
		CustomerService:  NewCustomerService(store, logger),
		AuthService:      NewAuthService(store, logger),
		CompaniesService: NewCompanyService(store, logger),
		ProductService:   NewProductService(store, logger),
	}
}
