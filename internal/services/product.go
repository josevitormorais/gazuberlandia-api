package services

import (
	"context"
	"errors"
	"gazuberlandia/internal/datastore"
	"gazuberlandia/internal/domain"
	"io"
	"mime/multipart"

	"cloud.google.com/go/storage"
	"github.com/gofrs/uuid"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type productService struct {
	store datastore.DataStore
	log   *zerolog.Logger
}

func NewProductService(store datastore.DataStore, logger *zerolog.Logger) *productService {
	newLog := logger.With().Str("Service", "[PRODUCT_SERVICE]").Logger()
	return &productService{store: store, log: &newLog}
}

func uploadFileOnBucket(ctx context.Context, fileName string, f *multipart.FileHeader, logger *zerolog.Logger) error {
	file, err := f.Open()
	if err != nil {
		logger.Error().Err(err).Msg("error trying open file to create a new product.")
		return err
	}
	defer func(file multipart.File) {
		if err := file.Close(); err != nil {
			logger.Error().Err(err).Msg("error when closing file.")
		}
	}(file)

	//TODO dynamic bucket, env or define on database per company
	bucket := "gazuberlandia"

	// TODO refactor storage to new package and new struct,can be up to one service injected
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		log.Error().Err(err).Msg("error when create a new storage client.")
		return err
	}

	sw := storageClient.Bucket(bucket).Object(fileName).NewWriter(ctx)

	if _, err := io.Copy(sw, file); err != nil {
		log.Error().Err(err).Msg("error when copy file to bucket.")
		return err
	}

	if err := sw.Close(); err != nil {
		log.Error().Err(err).Msg("error when closing storage client.")
		return err
	}

	return nil
}

func updateFileOnBucket(ctx context.Context, fileToUpdate string, newFileName string, f *multipart.FileHeader, logger *zerolog.Logger) error {

	file, err := f.Open()
	if err != nil {
		logger.Error().Err(err).Msg("error trying open file to create a new product.")
		return err
	}
	defer func(file multipart.File) {
		if err := file.Close(); err != nil {
			logger.Error().Err(err).Msg("error when closing file.")
		}
	}(file)

	//TODO dynamic bucket, env or define on database per company
	bucket := "gazuberlandia"

	// TODO refactor storage to new package
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		logger.Error().Err(err).Msg("error when create a new storage client.")
		return err
	}

	b := storageClient.Bucket(bucket)

	if err := b.Object(fileToUpdate).Delete(ctx); err != nil {
		logger.Error().Err(err).Msg("error when delete old image in storage.")
		return err
	}

	sw := b.Object(newFileName).NewWriter(ctx)

	if _, err := io.Copy(sw, file); err != nil {
		logger.Error().Err(err).Msg("error when save file in bucket.")
		return err
	}

	if err := sw.Close(); err != nil {
		logger.Error().Err(err).Msg("error when closing storage client.")
		return err
	}

	return nil
}

func (p *productService) CreateProduct(ctx context.Context, companyID uint, product *domain.Product, f *multipart.FileHeader) error {
	logger := domain.Logger(p.log, ctx)
	logger.Trace().Msg("stating create new product.")

	product.OriginalFileName = f.Filename
	product.SetIsNewProductFile()
	if err := product.SetNewFileName(f.Filename); err != nil {
		logger.Error().Err(err).Msg("error regex compile.")
		return err
	}

	err := p.store.CreateProduct(ctx, companyID, product)
	if err != nil {
		logger.Error().Err(err).Msg("error to create a new product.")
		return FormatErr(err)
	}

	err = uploadFileOnBucket(ctx, product.GetFileName(), f, logger)
	if err != nil {
		logger.Error().Msg("error to save file on bucket.")
		return err
	}

	return nil
}

func (p *productService) UpdateProduct(ctx context.Context, companyID uint, product *domain.UpdateProduct, f *multipart.FileHeader) error {
	logger := domain.Logger(p.log, ctx)
	logger.Trace().Msg("starting update product.")

	pdOnDatabase, err := p.store.FindProductById(ctx, product.ID, companyID)
	if err != nil {
		return FormatErr(err)
	}

	if pdOnDatabase == nil {
		return errors.New("product not found")
	}

	// TODO implementar o delete da antiga imagem do produto no storage, e fazer o novo upload.

	if f != nil {
		product.OriginalFileName = f.Filename
		product.SetIsNewProductFile()

		if err := product.SetNewFileName(f.Filename); err != nil {
			logger.Error().Err(err).Msg("error regex compile.")
			return err
		}

		err = updateFileOnBucket(ctx, pdOnDatabase.FileName, product.GetFileName(), f, logger)

		if err != nil {
			logger.Error().Msg("error to save file on bucket.")
			return errors.New("error to save file, please trying.")
		}
	}
	// TODO adicionar update primeiro no fluxo

	err = p.store.UpdateProduct(ctx, companyID, product)
	if err != nil {
		return FormatErr(err)
	}

	return nil
}

func (p *productService) DeleteProduct(ctx context.Context, id uuid.UUID) error {
	domain.Logger(p.log, ctx).Trace().Msg("delete product.")

	err := p.store.DeleteProduct(ctx, id)
	if err != nil {
		return FormatErr(err)
	}

	return nil
}

func (p *productService) FindProductById(ctx context.Context, id uuid.UUID, companyID uint) (*domain.ProductResponse, error) {
	domain.Logger(p.log, ctx).Trace().Msg("find product by id.")

	pd, err := p.store.FindProductById(ctx, id, companyID)
	if err != nil {
		return nil, FormatErr(err)
	}

	return domain.ProductResponseFromDataStore(*pd), nil
}

func (p *productService) GetAllProduct(ctx context.Context, companyID uint) (*[]domain.ProductResponse, error) {
	logger := domain.Logger(p.log, ctx)
	logger.Trace().Msg("get all product.")

	pd, err := p.store.GetAllProduct(ctx, companyID)
	if err != nil {
		return nil, FormatErr(err)
	}

	pds := domain.AllProducts(*pd)

	if pds == nil {
		logger.Info().Msg("not found products in database.")
		return &[]domain.ProductResponse{}, nil
	}

	return pds, nil
}

func (p *productService) SearchProducts(ctx context.Context, companyID, userID, page, pageSize uint, query string) (*[]domain.ProductResponse, error) {
	domain.Logger(p.log, ctx).Trace().Msg("search products.")

	pd, err := p.store.SearchProducts(ctx, companyID, userID, page, pageSize, query)
	if err != nil {
		return nil, FormatErr(err)
	}

	return domain.AllProducts(*pd), nil
}
func (p *productService) SearchProductsPaginated(ctx context.Context, userID, companyID, page, pageSize uint) (*[]domain.ProductResponse, error) {
	domain.Logger(p.log, ctx).Trace().Msg("pagination search products.")

	pd, err := p.store.SearchProductsPaginated(ctx, userID, companyID, page, pageSize)
	if err != nil {
		return nil, FormatErr(err)
	}

	return domain.AllProducts(*pd), nil
}
