package services

import (
	"context"
	"gazuberlandia/internal/datastore"
	"gazuberlandia/internal/domain"

	"github.com/rs/zerolog"
)

type customerService struct {
	store datastore.DataStore
	log   *zerolog.Logger
}

func NewCustomerService(store datastore.DataStore, logger *zerolog.Logger) *customerService {
	newLog := logger.With().Str("Service", "[CUSTOMER_SERVICE]").Logger()
	return &customerService{store: store, log: &newLog}
}

func (c *customerService) CreateCustomer(ctx context.Context, customer *domain.CreateCustomerRequest) error {
	domain.Logger(c.log, ctx).Trace().Msg("create a new customer.")

	err := c.store.CreateCustomer(ctx, customer)
	if err != nil {
		return FormatErr(err)
	}
	return nil
}

func (c *customerService) DeleteCustomer(ctx context.Context, id uint) error {
	domain.Logger(c.log, ctx).Trace().Uint("customer_id", id).Msg("delete customer.")

	err := c.store.DeleteCustomer(ctx, id)
	if err != nil {
		return FormatErr(err)
	}
	return nil
}
func (c *customerService) DeleteCustomerAddress(ctx context.Context, id uint) error {
	domain.Logger(c.log, ctx).Trace().Uint("address_id", id).Msg("delete address customer.")

	err := c.store.DeleteCustomerAddress(ctx, id)
	if err != nil {
		return FormatErr(err)
	}
	return nil
}

func (c *customerService) FindCustomerById(ctx context.Context, id uint) (*domain.Customer, error) {
	domain.Logger(c.log, ctx).Trace().Uint("customer_id", id).Msg("find customer by id.")

	customer, err := c.store.FindCustomerById(ctx, id)
	if err != nil {
		return nil, FormatErr(err)
	}

	return domain.CustomerResponseFromDataStore(*customer), nil
}

func (c *customerService) UpdateCustomer(ctx context.Context, customer *domain.Customer, companyID, id uint) error {
	domain.Logger(c.log, ctx).Trace().Uint("customer_id", id).Msg("update customer by id.")

	err := c.store.UpdateCustomer(ctx, customer, companyID, id)
	if err != nil {
		return FormatErr(err)
	}

	return nil
}

func (c *customerService) GetAllCustomer(ctx context.Context, companyID uint) (*[]domain.Customer, error) {
	domain.Logger(c.log, ctx).Trace().Msg("get all customers by company.")

	cs, err := c.store.GetAllCustomer(ctx, companyID)
	if err != nil {
		return nil, FormatErr(err)
	}

	cts := domain.AllCustomerFromStore(*cs)

	if cts == nil {
		return &[]domain.Customer{}, nil
	}

	return cts, nil
}

func (c *customerService) SearchCustomer(ctx context.Context, query domain.CustomerQueryParams, page, pageSize, companyID uint) (*[]domain.Customer, error) {
	domain.Logger(c.log, ctx).Trace().Uint("page", page).Uint("page_size", pageSize).Msg("search customer.")

	cs, err := c.store.SearchCustomer(ctx, query, page, pageSize, companyID)
	if err != nil {
		return nil, FormatErr(err)
	}

	cts := domain.AllCustomerFromStore(*cs)

	if cts == nil {
		return &[]domain.Customer{}, nil
	}

	return cts, nil
}

func (c *customerService) SearchCustomerPaginated(ctx context.Context, page, pageSize, companyID uint, withCalc bool) (*[]domain.Customer, error) {
	domain.Logger(c.log, ctx).Trace().Uint("page", page).Uint("page_size", pageSize).Msg("search customer by pagination.")

	cs, err := c.store.SearchCustomerPaginated(ctx, page, pageSize, companyID, withCalc)
	if err != nil {
		return nil, FormatErr(err)
	}

	cts := domain.AllCustomerFromStore(*cs)

	if cts == nil {
		return &[]domain.Customer{}, nil
	}

	return cts, nil
}
