package services

import (
	"context"
	"errors"
	"gazuberlandia/internal/datastore"
	"gazuberlandia/internal/domain"
	"strings"

	"github.com/alexedwards/argon2id"
	"github.com/rs/zerolog"
)

type authService struct {
	store datastore.DataStore
	log   *zerolog.Logger
}

func NewAuthService(store datastore.DataStore, logger *zerolog.Logger) *authService {
	newLog := logger.With().Str("Service", "[AUTH_SERVICE]").Logger()
	return &authService{store: store, log: &newLog}
}

func (u *authService) Login(ctx context.Context, email, password string, jwtSecret string) (string, error) {
	log := domain.Logger(u.log, ctx)
	log.Trace().Msg("user login.")

	user, err := u.store.FindUserByEmail(ctx, email, true)

	if err != nil {
		if strings.Contains(FormatErr(err).Error(), "not found") {
			log.Info().Msg("user not found on database.")
			return "", errors.New("email or password is invalid")
		}
		log.Error().Err(err).Msg("error trying to find user in database.")
		return "", FormatErr(err)
	}

	match, err := argon2id.ComparePasswordAndHash(password, user.Password)
	isEqual := strings.Compare(user.Email, email)

	if err != nil {
		log.Error().Err(err).Msg("error trying compare hash password.")
		return "", errors.New("internal server error")
	}

	if !match || isEqual != 0 {
		return "", errors.New("email or password is invalid")
	}

	return domain.GenerateJWT(user.ID, jwtSecret)
}
