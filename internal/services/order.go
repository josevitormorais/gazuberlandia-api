package services

import (
	"context"
	"gazuberlandia/internal/datastore"
	"gazuberlandia/internal/domain"

	"github.com/gofrs/uuid"
	"github.com/rs/zerolog"
)

type orderService struct {
	store datastore.DataStore
	log   *zerolog.Logger
}

func NewOrderService(store datastore.DataStore, logger *zerolog.Logger) *orderService {
	newLog := logger.With().Str("Service", "[ORDER_SERVICE]").Logger()
	return &orderService{store: store, log: &newLog}
}

func (o *orderService) CreateOrder(ctx context.Context, order *domain.Order, companyID uint) error {
	domain.Logger(o.log, ctx).Trace().Msg("create a new order.")

	order.HandleNewOrderStatus()
	order.SetSubTotalAmount()

	err := o.store.CreateOrder(ctx, order, companyID)
	if err != nil {
		domain.Logger(o.log, ctx).Error().Err(err).Msg("error when create a new order.")
		return FormatErr(err)
	}
	return nil
}

func (o *orderService) FindOrderByID(ctx context.Context, orderID uuid.UUID, userID, companyID uint) (*domain.OrderResponse, error) {
	domain.Logger(o.log, ctx).Trace().Msg("find order by id.")

	order, err := o.store.FindOrderByID(ctx, orderID, userID, companyID)
	if err != nil {
		domain.Logger(o.log, ctx).Error().Err(err).Msg("error trying create a new order.")
		return nil, FormatErr(err)
	}

	return domain.MapedOrder(*order), nil
}

func (o *orderService) GetAllOrder(ctx context.Context, userID, companyID uint) ([]domain.OrderResponse, error) {
	domain.Logger(o.log, ctx).Trace().Msg("get all order.")

	order, err := o.store.GetAllOrder(ctx, userID, companyID)
	if err != nil {
		domain.Logger(o.log, ctx).Error().Err(err).Msg("error get all order.")
		return nil, FormatErr(err)
	}

	if order == nil {
		domain.Logger(o.log, ctx).Warn().Msg("no order found.")
		return []domain.OrderResponse{}, nil
	}
	return domain.OrderResponseFromDataStore(order), nil
}

func (o *orderService) SearchOrderPaginated(ctx context.Context, page, pageSize, userID, companyID uint) ([]domain.OrderResponse, error) {
	domain.Logger(o.log, ctx).Trace().Uint("page", page).Uint("pageSize", pageSize).Msg("Search order paginated.")

	order, err := o.store.SearchOrderPaginated(ctx, page, pageSize, userID, companyID)
	if err != nil {
		domain.Logger(o.log, ctx).Error().Err(err).Msg("error trying to lookup paginated order.")
		return nil, FormatErr(err)
	}

	if order == nil {
		domain.Logger(o.log, ctx).Warn().Msg("no order found on search paginated.")
		return []domain.OrderResponse{}, nil
	}
	return domain.OrderResponseFromDataStore(order), nil
}

func (o *orderService) UpdateOrder(ctx context.Context, order *domain.OrderUpdated, orderID uuid.UUID, userID, companyID uint) error {
	domain.Logger(o.log, ctx).Trace().Str("order_id", orderID.String()).Msg("updating order.")

	order.HandleNewOrderStatus()
	order.SetSubTotalAmount()

	err := o.store.UpdateOrder(ctx, order, orderID, userID, companyID)

	if err != nil {
		domain.Logger(o.log, ctx).Error().Err(err).Msg("error when trying to update the order.")
		return FormatErr(err)
	}

	return nil
}
func (o *orderService) SearchOrder(ctx context.Context, q *domain.OrderQueryParams, page, pageSize, userID, companyID uint) ([]domain.OrderResponse, error) {
	domain.Logger(o.log, ctx).Trace().Str("query", q.Query).Uint("page", page).Uint("pageSize", pageSize).Msg("search order.")

	order, err := o.store.SearchOrder(ctx, q, page, pageSize, userID, companyID)
	if err != nil {
		domain.Logger(o.log, ctx).Error().Err(err).Msg("error when search order.")
		return nil, FormatErr(err)
	}

	if order == nil {
		domain.Logger(o.log, ctx).Warn().Msg("no order found on search.")
		return []domain.OrderResponse{}, nil
	}
	return domain.OrderResponseFromDataStore(order), nil
}

func (c *orderService) GetOrderCompanyInfo(ctx context.Context, userID, companyID uint) (*domain.CompanyOrdersInfoQty, error) {
	cpInfo, err := c.store.GetOrderCompanyInfo(ctx, userID, companyID)
	if err != nil {
		return nil, FormatErr(err)
	}
	return &domain.CompanyOrdersInfoQty{
		OrdersMonthQty:                     cpInfo.OrdersMonthQty,
		OrdersWeekQty:                      cpInfo.OrdersWeekQty,
		OrdersTodayQty:                     cpInfo.OrdersTodayQty,
		OrdersPaymentPendingQty:            cpInfo.OrdersPaymentPendingQty,
		OrdersPaymentCreditCardQty:         cpInfo.OrdersPaymentCreditCardQty,
		OrdersPaymentMoneyQty:              cpInfo.OrdersPaymentMoneyQty,
		OrdersPaymentDebitCardQty:          cpInfo.OrdersPaymentDebitCardQty,
		OrdersTotalAmountLastMonth:         cpInfo.OrdersTotalAmountLastMonth,
		OrdersTotalAmountLastWeek:          cpInfo.OrdersTotalAmountLastWeek,
		OrdersTotalAmountToday:             cpInfo.OrdersTotalAmountToday,
		OrdersTotalAmountPaymentCreditCard: cpInfo.OrdersTotalAmountPaymentCreditCard,
		OrdersTotalAmountPaymentMoney:      cpInfo.OrdersTotalAmountPaymentMoney,
		OrdersTotalAmountPaymentDebitCard:  cpInfo.OrdersTotalAmountPaymentDebitCard,
		OrdersTotalAmountPaymentPending:    cpInfo.OrdersTotalAmountPaymentPending,
		LastRegisteredOrders:               domain.OrderResponseFromDataStore(cpInfo.LastRegisteredOrders),
	}, nil
}
