package sessions

// type SessionStore struct {
// 	Codecs   []securecookie.Codec
// 	options  *sessions.Options
// 	sessions *sessions.Session
// 	store    *sqlx.DB
// 	Prefix   string
// }

// func NewSessionStore(ctx context.Context, db *sqlx.DB) *SessionStore {
// 	prefix := os.Getenv("SESSION_PREFIX")
// 	secret := []byte(os.Getenv("SESSION_SECRET"))
// 	expiresIn, _ := strconv.Atoi(os.Getenv("SESSION_EXPIRE"))

// 	return &SessionStore{
// 		Codecs: securecookie.CodecsFromPairs(secret),
// 		store:  db,
// 		Prefix: prefix,
// 		options: &sessions.Options{
// 			Path:     "/",
// 			MaxAge:   expiresIn,
// 			HttpOnly: true,
// 			Secure:   true,
// 		},
// 		sessions: &sessions.Session{},
// 	}
// }

// func (store *SessionStore) New(r *http.Request, name string) (*sessions.Session, error) {
// 	store.sessions = sessions.NewSession(store, name)
// 	store.sessions.Options = store.options

// 	cookie, err := r.Cookie(name)
// 	if err != nil {
// 		store.sessions.IsNew = true
// 		return store.sessions, err
// 	}

// 	store.sessions.IsNew = false
// 	store.sessions.ID = cookie.Value

// 	return store.sessions, nil
// }

// func (store *SessionStore) Get(r *http.Request, name string) (*sessions.Session, error) {
// 	if _, err := r.Cookie(name); err != nil {
// 		return nil, errors.New("session not found")
// 	}

// 	session, err := store.New(r, name)
// 	if err != nil {
// 		return session, err
// 	}

// 	err = store.load(r.Context(), session)

// 	if err != nil || errors.Is(err, redis.Nil) {
// 		return store.sessions, errors.New("session not found")
// 	}

// 	*r = *r.WithContext(gazuberlandia.NewContextWithSession(r.Context(), store.sessions))

// 	return store.sessions, nil
// }

// func (store *SessionStore) Save(r *http.Request, w http.ResponseWriter, s *sessions.Session) error {
// 	if store.options.MaxAge <= 0 {
// 		if _, err := store.Delete(r.Context(), s.ID); err != nil {
// 			return err
// 		}

// 		sessionID := ""
// 		http.SetCookie(w, sessions.NewCookie(s.Name(), sessionID, s.Options))
// 		return nil
// 	}

// 	if s.ID == "" {
// 		s.ID = strings.TrimRight(base32.StdEncoding.EncodeToString(securecookie.GenerateRandomKey(32)), "=")
// 	}

// 	if err := store.save(r.Context(), s); err != nil {
// 		return err
// 	}

// 	http.SetCookie(w, sessions.NewCookie(s.Name(), s.ID, s.Options))
// 	return nil
// }

// func (store *SessionStore) save(ctx context.Context, s *sessions.Session) error {
// 	b, err := securecookie.EncodeMulti(s.Name(), s.Values, store.Codecs...)
// 	if err != nil {
// 		return err
// 	}

// 	expiresIn := time.Duration(store.options.MaxAge) * time.Second
// 	scdm := store.store.Set(ctx, store.Prefix+s.ID, b, expiresIn)

// 	return scdm.Err()
// }

// func (store *SessionStore) load(ctx context.Context, s *sessions.Session) error {
// 	scmd := store.store.Get(ctx, store.Prefix+s.ID)

// 	if scmd.Err() != nil {
// 		return scmd.Err()
// 	}

// 	result, err := scmd.Result()

// 	if err != nil {
// 		return err
// 	}

// 	return securecookie.DecodeMulti(s.Name(), result, &s.Values, store.Codecs...)
// }

// func (store *SessionStore) Delete(ctx context.Context, sessionID string) (int64, error) {
// 	return store.store.Del(ctx, store.Prefix+sessionID).Result()
// }
