package main

//go:generate sqlboiler --flags-go-here psql

import (
	"context"
	"fmt"
	"gazuberlandia/internal/config"
	"gazuberlandia/internal/datastore/postgres"
	serverHttp "gazuberlandia/internal/transport/http"
	"net/http"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/caarlos0/env/v6"
	_ "github.com/jackc/pgx/v4"
	"github.com/rs/zerolog"
)

func main() {
	ctx := context.Background()
	logger := zerolog.New(os.Stdout).With().Timestamp().Logger()

	conf := &config.Config{}
	if err := env.Parse(conf); err != nil {
		logger.Error().Err(err).Msg("[SETUP] error to trying parse envs for struct")
	}

	pg, err := postgres.NewPostgres(ctx, conf.PostgresUrl(), &logger)

	if err != nil {
		logger.Fatal().Msg("[SETUP] Error connecting to database")
		os.Exit(1)
	}

	defer pg.Close()

	if conf.ENVIRONMENT != "Prod" {
		d, _ := os.Getwd()
		go func() {
			fPath := fmt.Sprintf("file:%s", path.Join(filepath.Dir(d), "internal/datastore/postgres/migrations"))
			 err := pg.RunMigrations(fPath, &logger, conf)
			 
			 if err != nil && !strings.Contains(strings.ToLower(err.Error()), "no change") {
				logger.Error().Err(err).Msg("[SETUP] error when running migrations")
			}
		}()
	}

	server := serverHttp.NewServer(
		conf,
		serverHttp.NewRouter(&logger),
		serverHttp.NewConfigHttpServer(),
		serverHttp.AddServices(pg, &logger),
		serverHttp.NewValidator(),
	)

	go func() {
		err = server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			logger.Error().Err(err).Msg("[SETUP] Error configuring server to up.")
		}
	}()

	fmt.Println(" ==> Server started on Port <==", server.Addr)

	downServerChan := make(chan os.Signal, 1)
	signal.Notify(downServerChan, syscall.SIGINT, syscall.SIGTERM)

	sig := <-downServerChan
	logger.Trace().Str("sig", sig.String()).Msg("[DOWN] Server down.")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		logger.Error().Err(err).Msg("[DOWN] Server failed.")
	}

	logger.Trace().Msg("Server Exited.")
}
