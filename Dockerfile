# syntax=docker/dockerfile:1

#FROM golang:1.18.4-buster as build
#
#WORKDIR /app
#
#COPY go.* ./
#RUN go mod download && go mod verify
#
#COPY . ./
#
#RUN go build -o server ./cmd/main.go
#
#FROM debian:buster-slim
#
#RUN set -x && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
#    ca-certificates && \
#    rm -rf /var/lib/apt/lists/*
#
#COPY --from=build /server /server
#
#ENTRYPOINT ["/server"]

FROM golang:1.18.4-buster as builder

# Create and change to the app directory.
WORKDIR /app

COPY go.* ./
RUN go mod download

COPY . ./

# Build the binary.
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -mod=readonly -v -o server ./cmd/main.go

FROM gcr.io/distroless/base

COPY --from=builder /app/server /app/server

CMD ["/app/server"]